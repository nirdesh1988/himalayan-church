<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExtendedCategoryBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extended_category_branches', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('category_id')->nullable()->comment('menu category id')->unsigned();
            $table->string('title')->nullable()->comment('category title');
            $table->string('slug')->nullable()->comment('category slug');
            $table->string('short_information', 500)->nullable()->comment('short information');
            $table->longText('contents')->nullable()->comment('page contents');
            $table->string('featured_image')->nullable()->comment('featured image');
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('menu_item_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extended_category_branches');
    }
}
