<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable()->comment('user name');
            $table->string('address')->nullable()->comment('user address');
            $table->string('contact')->unique()->comment('user contact');
            $table->string('email')->unique()->comment('user email');
            $table->timestamp('email_verified_at')->nullable()->comment('user verification');
            $table->string('password')->nullable()->comment('user password');
            $table->string('avatar')->nullable()->comment('user avatar');
            $table->enum('status', ['Approved', 'Pending', 'Blocked'])->nullable()->default('Pending')->comment('user status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
