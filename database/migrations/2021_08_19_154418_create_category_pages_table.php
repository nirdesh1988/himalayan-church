<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_pages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('category_id')->nullable()->comment('menu category id')->unsigned();
            $table->longText('contents')->nullable()->comment('page contents');
            $table->string('featured_image')->nullable()->comment('featured image');
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('menu_item_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_pages');
    }
}
