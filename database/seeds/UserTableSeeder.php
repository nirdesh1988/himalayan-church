<?php

namespace Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loginDetails = [
            [
                'name' => 'Iswor L Shrestha',
                'address' => 'Woolwich, SE London',
                'contact' => '7818320770',
                'status' => 'Approved',
                'email' => 'merodesh2012@gmail.com',
                'password' => Hash::make('DEADhen500'),
            ],
            [
                'name' => 'Dipendra Maharjan',
                'address' => 'Gwarko, Lalitpur',
                'contact' => '9851457812',
                'status' => 'Approved',
                'email' => 'anarakihasu@gmail.com',
                'password' => Hash::make('DEADhen500'),
            ],
        ];
        foreach ($loginDetails as $loginDetail) {
            User::create($loginDetail);
        }
    }
}
