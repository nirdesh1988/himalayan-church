<?php

use App\Models\SystemConfiguration;
use Illuminate\Support\Facades\Cache;

class SystemConfigs
{
    public static function getConfig(string $key, $default = null)
    {
        self::refresh();
        $config = self::getConfigCache();
        return $config[$key] ?? $default;
    }

    private static function getConfigCache()
    {
        return Cache::rememberForever('system_config', function () {
            return (SystemConfiguration::select('name', 'value')->get()->keyBy('name')->pluck('value', 'name')->toArray());
        });
    }

    public static function refresh()
    {
        Cache::forget('system_config');
        return self::getConfigCache();
    }

}
