<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verses extends Model
{
    protected $fillable = [
        'verse', 'reference', 'display'
    ];
}
