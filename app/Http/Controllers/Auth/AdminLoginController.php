<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminLoginController extends Controller
{

    public function showLoginForm()
    {
        return $this->returnBackendView('login', ['page_title' => 'Login']);
    }

    public function showForgetPasswordForm()
    {
        return $this->returnBackendView('forget-password', ['page_title' => 'Forget Password']);
    }

    public function login(Request $request)
    {
        $username = $request->get('login-username');
        $password = $request->get('login-password');

        $user = User::where('email', $username)->first();
        if ($user) {
            if ($user->status == 'Blocked') {
                return redirect()->back()->with('error', 'Your account is BLOCKED. Please, contact to administrator!');
            }
            if ($user->status == 'Pending') {
                return redirect()->back()->with('error', 'Your account is still Pending. Please, contact to administrator!');
            }
            if (password_verify($password, $user->password)) {
                Session::put('username', $username);
                Session::put('password', $password);

                if (Auth::attempt(['email' => Session::get('username'), 'password' => Session::get('password'), 'status' => 'Approved'])) {
                    return redirect()->route('dashboard');
                } else {
                    return redirect()->back()->with('error', 'Invalid email or password. Please! try again.');
                }
            } else {
                return redirect()->back()->with('error', 'Invalid Password, please try again!');
            }
        } else {
            return redirect()->back()->with('error', 'Invalid Email ID, please try again!');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('admin.login')->with('status', 'Admin has been logged out!');
    }
}
