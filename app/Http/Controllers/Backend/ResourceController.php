<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\CategoryPage;
use App\Models\ExtendedCategoryBranch;
use App\Models\MenuItemCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ResourceController extends Controller
{
    private $_page = 'resources.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = MenuItemCategory::where('main_menu', 'resource')->orderBy('created_at', 'desc')->get();
        return $this->returnBackendView($this->_page . 'index', ['page_title' => 'resources', 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return $this->returnBackendView($this->_page . 'details', ['page_title' => 'resources', 'slug' => 'video sermons']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function details($slug, $id)
    {
        $data = MenuItemCategory::find($id);
        $branchData = ExtendedCategoryBranch::where('category_id', $id)->get();
        return $this->returnBackendView($this->_page . 'details', ['page_title' => 'Update ', 'slug' => $slug, 'menuData' => $data, 'branchData' => $branchData]);
    }

    public function addResources($slug, $id)
    {
        return $this->returnBackendView($this->_page . 'add', ['page_title' => 'Add', 'slug' => $slug, 'id' => $id]);
    }

    public function editResources($category, $slug, $id)
    {
        $branchData = ExtendedCategoryBranch::find($id);
        return $this->returnBackendView($this->_page . 'edit', ['page_title' => 'Edit', 'data' => $branchData, 'category' => $category, 'slug' => $branchData->title, 'id' => $id]);
    }

    public function storeBranches(Request $request, $slug, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'short_info' => 'required',
            'contents' => 'nullable',
            'image' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:10240',
        ]);

        if ($validator->fails()) {
            return response(array("success" => false, "message" => "validation failed", "errors" => $validator->errors()), 422);
        }

        if ($request->file('image') != '') {
            $path = 'images/category/';
            $image = $request->file('image');
            $rename = Str::uuid();
            $new_name = $path . $rename . '.' . $image->getClientOriginalExtension();

            $data = [
                'category_id' => $id,
                'title' => $request->get('title'),
                'slug' => Str::slug($request->get('title')),
                'short_information' => $request->get('short_info'),
                'contents' => $request->get('contents'),
                'featured_image' => $new_name
            ];
            $result = ExtendedCategoryBranch::create($data);
            if ($result) {
                $image->move($path, $new_name);
                return redirect()->back()->with(['success' => "Page contents successfully added"]);
            } else {
                return redirect()->back()->with(['error' => "Page contents failed to add. Please, try again"]);
            }
        } else {
            $data = [
                'category_id' => $id,
                'title' => $request->get('title'),
                'slug' => Str::slug($request->get('title')),
                'short_information' => $request->get('short_info'),
                'contents' => $request->get('contents'),
            ];
            $result = ExtendedCategoryBranch::create($data);
            if ($result) {
                return redirect()->back()->with(['success' => "Page contents successfully added"]);
            } else {
                return redirect()->back()->with(['error' => "Page contents failed to add. Please, try again"]);
            }
        }

    }

    public function updateBranches(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'short_info' => 'required',
            'contents' => 'nullable',
            'image' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:10240',
        ]);

        if ($validator->fails()) {
            return response(array("success" => false, "message" => "validation failed", "errors" => $validator->errors()), 422);
        }

        if ($request->file('image') != '') {
            $path = 'images/category/';
            $image = $request->file('image');
            $rename = Str::uuid();
            $new_name = $path . $rename . '.' . $image->getClientOriginalExtension();

            $data = [
                'title' => $request->get('title'),
                'slug' => Str::slug($request->get('title')),
                'short_information' => $request->get('short_info'),
                'contents' => $request->get('contents'),
                'featured_image' => $new_name
            ];
            $result = ExtendedCategoryBranch::where('id', $id)->update($data);
            if ($result) {
                $image->move($path, $new_name);
                return redirect()->back()->with(['success' => "Page contents successfully update"]);
            } else {
                return redirect()->back()->with(['error' => "Page contents failed to add. Please, try again"]);
            }
        } else {
            $data = [
                'title' => $request->get('title'),
                'slug' => Str::slug($request->get('title')),
                'short_information' => $request->get('short_info'),
                'contents' => $request->get('contents'),
            ];
            $result = ExtendedCategoryBranch::where('id', $id)->update($data);
            if ($result) {
                return redirect()->back()->with(['success' => "Page contents successfully update"]);
            } else {
                return redirect()->back()->with(['error' => "Page contents failed to add. Please, try again"]);
            }
        }

    }
}
