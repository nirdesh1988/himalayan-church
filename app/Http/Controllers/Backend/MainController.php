<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\DbBackup;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Support\Str;

class MainController extends Controller
{

    public function index()
    {
        return $this->returnBackendView('dashboard', ['page_title' => 'Dashboard']);
    }

    public function dispayData()
    {
        $data = DbBackup::latest()->get();
        return $this->returnBackendView('db-backup', ['page_title' => 'DB Backup', 'data' => $data]);
    }

    public function exportBackup()
    {
        $filename = "db-backup-" . Str::uuid() . '-' . Carbon::now()->format('Y-m-d') . ".gz";
        $command = "mysqldump --column-statistics=0 --user=" . env('DB_USERNAME') . " --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . "  | gzip > " . storage_path() . "/app/public/backup/" . $filename;
        $returnVar = NULL;
        $output = NULL;
        exec($command, $output, $returnVar);
        DbBackup::create([
            'file_name' => $filename
        ]);
        return redirect()->back()->with(['message' => 'DB backup successfully created']);
    }

    function downloadFile($filename)
    {
        $file = public_path('storage/backup/') . $filename;
        return response()->download($file);
    }
}
