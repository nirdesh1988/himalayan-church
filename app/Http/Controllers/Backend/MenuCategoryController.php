<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\CategoryPage;
use App\Models\MenuItemCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MenuCategoryController extends Controller
{
    private $_page = 'category.';
    private $_page_resource = 'resources.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($data, $id = null)
    {
        if ($id != null) {
            return $this->returnBackendView($this->_page . 'index', ['page_title' => 'Update Menu Category', 'data' => $data, 'category' => $id]);
        } else {
            return $this->returnBackendView($this->_page . 'index', ['page_title' => 'Add Menu Category', 'data' => $data, 'category' => null]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($menu, $slug, $id)
    {
        $data = MenuItemCategory::find($id);
        $pageContent = CategoryPage::where('category_id', $id)->first();
        return $this->returnBackendView($this->_page . 'show', ['page_title' => $menu . ' Details', 'data' => $menu, 'slug' => $slug, 'menuData' => $data, 'pageContent' => $pageContent]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->get('id');
        $dataExists = CategoryPage::where('category_id', $id)->first();
        if ($dataExists == null) {
            //insert page content
            $validator = Validator::make($request->all(), [
                'contents' => 'required',
                'image' => 'required|image|mimes:jpg,jpeg,png,svg,gif|max:10240',
            ]);

            if ($validator->fails()) {
                return response(array("success" => false, "message" => "validation failed", "errors" => $validator->errors()), 422);
            }

            if ($request->file('image') != '') {
                $path = 'images/category/';
                $image = $request->file('image');
                $rename = Str::uuid();
                $new_name = $path . $rename . '.' . $image->getClientOriginalExtension();

                $data = [
                    'category_id' => $id,
                    'contents' => $request->get('contents'),
                    'featured_image' => $new_name
                ];
                $result = CategoryPage::create($data);
                if ($result) {
                    $image->move($path, $new_name);
                    return redirect()->back()->with(['success' => "Page contents successfully updated"]);
//                    return response(array("success" => true, "message" => "Page contents successfully added"), 200);
                } else {
                    return redirect()->back()->with(['error' => "Page contents failed to add. Please, try again"]);
//                    return response(array("success" => false, "message" => "Page contents failed to add. Please, try again"), 422);
                }
            } else {
                return redirect()->back()->with(['error' => "Please, upload featured image"]);
//                return response(array("success" => false, "message" => "Please, upload featured image"), 422);
            }
        } else {
            //update page content
            $validator = Validator::make($request->all(), [
                'contents' => 'required',
            ]);

            if ($validator->fails()) {
                return response(array("success" => false, "message" => "validation failed", "errors" => $validator->errors()), 422);
            }
            if ($request->file('image') != '') {
                $path = 'images/category/';
                $image = $request->file('image');
                $rename = Str::uuid();
                $new_name = $path . $rename . '.' . $image->getClientOriginalExtension();

                $data = [
                    'contents' => $request->get('contents'),
                    'featured_image' => $new_name
                ];

                //delete existing category image
                $categoryImage = CategoryPage::where('category_id', $id)->first();
                if ($categoryImage) {
                    if (file_exists($categoryImage->featured_image)) {
                        unlink($categoryImage->featured_image);
                    }
                }

                $result = CategoryPage::where('category_id', $id)->update($data);
                if ($result) {
                    $image->move($path, $new_name);
                    return redirect()->back()->with(['success' => "Page contents successfully updated"]);
//                    return response(array("success" => true, "message" => "Page contents successfully updated"), 200);
                } else {
                    return redirect()->back()->with(['error' => "Something went wrong. Please, try again"]);
//                    return response(array("success" => false, "message" => "Something went wrong. Please, try again"), 422);
                }
            } else {
                $data = [
                    'contents' => $request->get('contents'),
                ];
                $result = CategoryPage::where('category_id', $id)->update($data);
                if ($result) {
                    return redirect()->back()->with(['success' => "Page contents successfully updated"]);
//                    return response(array("success" => true, "message" => "Page contents successfully updated"), 200);
                } else {
                    return redirect()->back()->with(['error' => "Something went wrong. Please, try again"]);
//                    return response(array("success" => false, "message" => "Something went wrong. Please, try again"), 422);
                }
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->get('id');

        $result = MenuItemCategory::where('id', $id)->delete();
        if ($result) {
            return response(array("success" => true, "message" => "Menu category successfully deleted"), 200);
        } else {
            return response(array("success" => false, "message" => "Something went wrong. Please, try again"), 422);
        }

    }

}
