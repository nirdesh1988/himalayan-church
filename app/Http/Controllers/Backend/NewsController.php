<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    private $_page = 'news.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->returnBackendView($this->_page . 'index', ['page_title' => 'news & articles']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->returnBackendView($this->_page . 'create', ['page_title' => 'add news & articles']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'short_info' => 'required|max:255',
//            'description' => 'required',
            'image' => 'required|image|mimes:jpg,jpeg,png,svg,gif|max:10240',
        ]);

        if ($validator->fails()) {
            return response(array("success" => false, "message" => "validation failed", "errors" => $validator->errors()), 422);
        }

        if ($request->file('image') != '') {
            $path = 'images/news/';
            $image = $request->file('image');
            $rename = md5(rand(111, 9999999));
            $new_name = $path . $rename . '.' . $image->getClientOriginalExtension();

            $data = [
                'title' => $request->get('title'),
                'short_info' => $request->get('short_info'),
                'description' => $request->get('description'),
                'image' => $new_name,
            ];
            $result = News::create($data);
            if ($result) {
                $image->move($path, $new_name);
                return redirect()->back()->with(['success' => "News successfully updated"]);
//                return response(array("success" => true, "message" => "Something went wrong. Please, try again"), 200);
            } else {
                return redirect()->back()->with(['error' => "Something went wrong. Please, try again"]);
//                return response(array("success" => false, "message" => "Something went wrong. Please, try again"), 422);
            }
        } else {
            return redirect()->back()->with(['error' => "Please, upload featured image"]);
//            return response(array("success" => false, "message" => "Please, upload featured image"), 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $newsData = News::find($id);
        return $this->returnBackendView($this->_page . 'update', ['page_title' => 'update news & articles', 'newsData' => $newsData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->get('id');

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'short_info' => 'required|max:255',
//            'description' => 'required',
            'image' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:10240',
        ]);

        if ($validator->fails()) {
            return response(array("success" => false, "message" => "validation failed", "errors" => $validator->errors()), 422);
        }

        if ($request->file('image') != '') {
            $path = 'images/news/';
            $image = $request->file('image');
            $rename = md5(rand(111, 9999999));
            $new_name = $path . $rename . '.' . $image->getClientOriginalExtension();

            $data = [
                'title' => $request->get('title'),
                'short_info' => $request->get('short_info'),
                'description' => $request->get('description'),
                'image' => $new_name,
            ];

            $newsImage = News::find($id);
            if ($newsImage) {
                if (file_exists($newsImage->image)) {
                    unlink($newsImage->image);
                }
            }

            $result = News::where('id', $id)->update($data);
            if ($result) {

                $image->move($path, $new_name);
                return redirect()->back()->with(['success' => "News successfully updated"]);
//                return response(array("success" => true, "message" => "Something went wrong. Please, try again"), 200);
            } else {
                return redirect()->back()->with(['error' => "Something went wrong. Please, try again"]);
//                return response(array("success" => false, "message" => "Something went wrong. Please, try again"), 422);
            }
        } else {
            $data = [
                'title' => $request->get('title'),
                'short_info' => $request->get('short_info'),
                'description' => $request->get('description'),
            ];
//            dd($data);
            $result = News::where('id', $id)->update($data);
            if ($result) {
                return redirect()->back()->with(['success' => "News successfully updated"]);
//                return response(array("success" => true, "message" => "News successfully updated"), 200);
            } else {
                return redirect()->back()->with(['error' => "Something went wrong. Please, try again"]);
//                return response(array("success" => false, "message" => "Something went wrong. Please, try again"), 422);
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
