<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    /**
     *
     * Return Backend view based on  name specify in $name variable
     *
     * @param string $name
     * @param array $data
     *
     * @return \Illuminate\View\View
     */
    public function returnBackendView(string $name, array $data): \Illuminate\View\View
    {
        return view('Backend.pages.' . $name, $data);
    }
}
