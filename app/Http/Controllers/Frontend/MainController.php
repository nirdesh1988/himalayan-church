<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Mail\EnquiryMail;
use App\Models\CategoryPage;
use App\Models\ExtendedCategoryBranch;
use App\Models\Gallery;
use App\Models\Leader;
use App\Models\MenuItemCategory;
use App\Models\News;
use App\Models\Testimonials;
use App\Models\Verses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
    private $_page = 'Frontend.pages.';

    public static function getMenuItem($menu = null)
    {
        if ($menu != null) {
            return MenuItemCategory::where('main_menu', $menu)->get();
        }
        return MenuItemCategory::get();
    }

    public static function getNews($news = null)
    {
        if ($news != null) {
            return News::where('id', $news)->get();
        }
        return News::latest()->get()->take(5);
    }

    public static function getGallery($limit = null, $footer = null)
    {
        if ($limit != null && $footer != null) {
            return Gallery::latest()->get()->take($limit);
        }
        return Gallery::orderBy('created_at', 'desc')->get();
    }

    public function index()
    {
        $verse = Verses::where('display', 'Y')->first();
        $leader = Leader::get();
        $testimonial = Testimonials::get();
        return view($this->_page . 'home')->with(['verse' => $verse, 'leader' => $leader, 'testimonial' => $testimonial, 'menuCategory' => self::getMenuItem(), 'newsData' => self::getNews(), 'footerGallery' => self::getGallery(6, 1)]);
    }

//    public function about()
//    {
//        return view($this->_page . 'about')->with(['menuCategory' => self::getMenuItem()]);
//    }

    public function gallery()
    {
        return view($this->_page . 'gallery')->with(['menuCategory' => self::getMenuItem(''), 'galleryData' => self::getGallery(), 'footerGallery' => self::getGallery(6, 1)]);
    }

    public function newsletter()
    {
        return view($this->_page . 'newsletter')->with(['menuCategory' => self::getMenuItem(), 'footerGallery' => self::getGallery(6, 1)]);
    }

    public function contact()
    {
        return view($this->_page . 'contact')->with(['menuCategory' => self::getMenuItem(), 'footerGallery' => self::getGallery(6, 1)]);
    }

    public function enquiryMail(Request $request)
    {
        // Form validation
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        ]);
        $details = ([
            'name' => ucwords(trim($request->get('name'))),
            'email' => $request->get('email'),
            'subject' => $request->get('subject'),
            'message' => $request->get('message'),
        ]);

        if (Mail::to('merodesh2012@gmail.com')->send(new EnquiryMail($details))) {
            return redirect()->back()->with(['success' => 'Your enqiury message successfully sent. Our representative will get in touch with you soon. Thank You!']);
        } else {
            return redirect()->back()->with(['error' => 'Something went wrong. Failed to your message. Please, try again']);
        }

    }

    public function categoryPages($slug)
    {
        $data = MenuItemCategory::where('slug', $slug)->first();
        $pageData = CategoryPage::where('category_id', $data->id)->first();
        return view($this->_page . 'category-pages')->with(['menuCategory' => self::getMenuItem(), 'pageContent' => $pageData, 'menuItem' => $data, 'footerGallery' => self::getGallery(6, 1)]);
    }

    public function resourcesBranch($slug)
    {
        $data = MenuItemCategory::where('slug', $slug)->first();
        $branchData = ExtendedCategoryBranch::where('category_id', $data->id)->get();
        return view($this->_page . 'resources-branch')->with(['menuCategory' => self::getMenuItem(), 'branchData' => $branchData, 'menuItem' => $data, 'footerGallery' => self::getGallery(6, 1)]);
    }
}
