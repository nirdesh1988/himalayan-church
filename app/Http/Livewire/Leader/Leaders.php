<?php

namespace App\Http\Livewire\Leader;

use App\Models\Gallery as ImageGallery;
use App\Models\Leader;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Leaders extends Component
{

    use WithFileUploads, WithPagination;

    public $page_title = 'Church Leaders';
    public $is_leader_show = 'list';

    public $leaderId, $name, $position, $information, $image, $data, $leaderImage;

    public $imageFileLocation = 'images/leaders/';

    protected $rules = [
        'name' => 'required|min:2',
        'position' => 'required|min:2',
        'information' => 'required|min:2',
        'image' => 'required|image|mimes:jpg,jpeg,png,svg,gif|max:10240',
    ];

    public function resetFormFields()
    {
        $this->name = '';
        $this->position = '';
        $this->information = '';
        $this->image = '';
    }

    // function is for removie all the error message and validation
    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function closeForm()
    {
        $this->emit('leaderDelete');
        $this->resetFormFields();
        $this->hydrate();
        $this->is_leader_show = 'list';
    }

    public function create()
    {
        $this->page_title = 'Add Church Leaders';
        $this->is_leader_show = 'create';
    }

    public function store()
    {
        $this->validate();

        $imageName = $this->image->store($this->imageFileLocation, 'public');
        Leader::create([
            'name' => $this->name,
            'position' => $this->position,
            'information' => $this->information,
            'image' => $imageName
        ]);

        session()->flash('message', 'Leaders details successfully added.');
        $this->closeForm();

    }

    public function edit($id)
    {
        $this->page_title = 'Update Church Leaders';
        $this->is_leader_show = 'edit';

        $this->leaderId = $id;
        $data = Leader::where('id', $this->leaderId)->first();
        $this->name = $data->name;
        $this->position = $data->position;
        $this->information = $data->information;
        $this->leaderImage = $data->image;
    }

    public function update()
    {
        $this->validate([
            'name' => 'required|min:2',
            'position' => 'required|min:2',
            'information' => 'required|min:2',
        ]);

        if ($this->image == null) {
            $data = [
                'name' => $this->name,
                'position' => $this->position,
                'information' => $this->information,
            ];
        } else {
            $oldImage = Leader::find($this->leaderId);
            if (file_exists('storage/' . $oldImage->image)) {
                unlink('storage/' . $oldImage->image);
            }

            $imageName = $this->image->store($this->imageFileLocation, 'public');
            $data = [
                'name' => $this->name,
                'position' => $this->position,
                'information' => $this->information,
                'image' => $imageName
            ];
        }

        Leader::where('id', $this->leaderId)->update($data);
        session()->flash('message', 'Leaders details successfully updated.');
        $this->closeForm();

    }

    public function delete($id)
    {
        $this->leaderId = $id;
    }

    public function destroy()
    {
        $leaderImage = Leader::where('id', $this->leaderId)->first();
        if ($leaderImage) {
            if (file_exists('storage/' . $leaderImage->image)) {
                unlink('storage/' . $leaderImage->image);
            }
            Leader::where('id', $this->leaderId)->delete();

            session()->flash('message', 'Leader details successfully deleted.');
        } else {
            session()->flash('message', 'Something went wrong. Please try again.');
        }
        $this->closeForm();
    }

    public function render()
    {
        $this->data = Leader::get();
        return view('livewire.leader.leaders');
    }
}
