<?php

namespace App\Http\Livewire\News;

use App\Models\News;
use Livewire\Component;

class NewsEvents extends Component
{
    public $page_title = 'News & Events';
    public $data, $newsId;


    public function resetFormFields()
    {
        $this->newsId = '';
    }

    // function is for removie all the error message and validation
    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function closeForm()
    {
        $this->emit('newsDelete'); //TO CLOSE THE MODAL FORM. "reviewStore" =>its a event name defined in the jquery in livewire component
        $this->resetFormFields();
        $this->hydrate();
    }

    public function delete($id)
    {
        $this->newsId = $id;
    }

    public function destroy()
    {
//        if (Gate::allows('isAdmin')) {
        $newsImage = News::where('id', $this->newsId)->first();
        if ($newsImage) {
            if (file_exists($newsImage->image)) {
                unlink($newsImage->image);
            }
            News::where('id', $this->newsId)->delete();
            session()->flash('success', 'News successfully deleted.');
        } else {
            session()->flash('error', 'Something went wrong. Please try again.');
        }
        $this->closeForm();
//        } else {
//            session()->flash('message', 'Sorry! you don\' have access to this action. Thanks');
//            $this->closeForm();
//        }

    }


    public function render()
    {
        $this->data = News::orderBy('created_at', 'desc')->get();
        return view('livewire.news.news-events');
    }
}
