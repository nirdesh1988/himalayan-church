<?php

namespace App\Http\Livewire\News;

use App\Models\News;
use Livewire\Component;
use Livewire\WithFileUploads;

class AddNewsEvents extends Component
{

    use WithFileUploads;

    public $page_title = 'Add News & Events';

    public $newsId, $title, $image, $short_info, $description, $data;
    public $imageFileLocation = 'images/news/';

    protected $rules = [
        'title' => 'required|min:2',
        'short_info' => 'required|min:2|max:200',
        'description' => 'required|min:2',
        'image' => 'required|image|mimes:jpg,jpeg,png,svg,gif|max:10240',
    ];

    public function resetFormFields()
    {
        $this->title = '';
        $this->image = '';
        $this->short_info = '';
        $this->description = '';
    }

    // function is for removie all the error message and validation
    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function closeForm()
    {
        $this->resetFormFields();
        $this->hydrate();
    }

    public function store()
    {
        $this->validate();

        if ($this->image != '') {
            $imageName = $this->image->store($this->imageFileLocation, 'public');
        }
        $data = new News([
            'title' => $this->title,
            'short_info' => $this->short_info,
            'description' => $this->description,
            'image' => $imageName,
        ]);
        $data->save();

        session()->flash('message', 'News successfully created.');
        $this->closeForm();
    }

    public function render()
    {
        return view('livewire.news.add-news-events');
    }
}
