<?php

namespace App\Http\Livewire\Config;

use App\Models\SystemConfiguration;
use Livewire\Component;

class SystemConfig extends Component
{
    public $page_title = 'System Configuration';
    public $name, $value, $data;

    protected $rules = [
        'name' => 'required|min:2',
        'value' => 'required|min:2',
    ];

    public function resetFormFields()
    {
        $this->name = '';
        $this->value = '';
    }

    // function is for removie all the error message and validation
    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function closeForm()
    {
        $this->emit('primaryStore');
        $this->emit('socialStore');
        $this->emit('seoStore');
        $this->resetFormFields();
        $this->hydrate();
    }

    public function storePrimary()
    {
        $this->validate();
        $dataExists = SystemConfiguration::where('name', $this->name)->first();
        if ($dataExists) {
            session()->flash('error', 'Primary configuration data already added. Thank you.');
        } else {
            $data = [
                'name' => $this->name,
                'value' => $this->value,
                'description' => 'primary'
            ];
            SystemConfiguration::create($data);
            session()->flash('message', 'Primary configuration data successfully added.');
        }

        $this->closeForm();
    }

    public function storeSocial()
    {
        $this->validate();

        $dataExists = SystemConfiguration::where('name', $this->name)->first();
        if ($dataExists) {
            session()->flash('error', 'Social connection already added. Thank you.');
        } else {
            $data = [
                'name' => $this->name,
                'value' => $this->value,
                'description' => 'social'
            ];
            SystemConfiguration::create($data);
            session()->flash('message', 'Social connection successfully added.');
        }
        $this->closeForm();
    }

    public function storeSeo()
    {
        $this->validate();

        $dataExists = SystemConfiguration::where('name', $this->name)->first();
        if ($dataExists) {
            session()->flash('error', 'Meta data already added. Thank you.');
        } else {
            $data = [
                'name' => $this->name,
                'value' => $this->value,
                'description' => 'seo'
            ];
            SystemConfiguration::create($data);
            session()->flash('message', 'Meta data successfully added.');
        }
        $this->closeForm();


    }

    public function render()
    {
        $this->data = SystemConfiguration::get();
        return view('livewire.config.system-config');
    }
}
