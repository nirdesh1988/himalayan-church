<?php

namespace App\Http\Livewire\Testimonials;

use App\Models\Testimonials;
use Livewire\Component;

class Testimonial extends Component
{
    public $isUpdateMode = 'list';
    public $data;
    public $reviewId, $name, $position, $review;

    public function render()
    {
        $this->data = Testimonials::get();
        return view('livewire.testimonials.testimonial');
    }

    public function resetFormFields()
    {
        $this->name = '';
        $this->position = '';
        $this->review = '';
    }

    public function resetFormback()
    {
        $this->isUpdateMode = 'list';
        $this->resetFormFields();
    }

    // function is for removie all the error message and validation
    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function closeForm()
    {
        $this->isUpdateMode = 'list';
        $this->resetFormback();
        $this->hydrate();
    }

    public function create()
    {
        $this->isUpdateMode = 'create';
    }

    protected $rules = [
        'name' => 'required|min:3',
        'position' => 'required|min:3',
        'review' => 'required|min:3',
    ];

    protected $messages = [
        'name.required' => 'Name cannot be empty.',
        'name.min' => 'Minimum 3 chars.',

        'position.required' => 'Position cannot be empty.',
        'position.min' => 'Minimum 3 chars.',

        'review.required' => 'Review cannot be empty.',
        'review.min' => 'Minimum 3 chars.',
    ];

    public function store()
    {
        $this->validate();

        // only if validation get success
        $data = new Testimonials([
            'name' => $this->name,
            'position' => $this->position,
            'review' => $this->review,
        ]);
        $data->save();
        session()->flash('message', 'Review successfully saved.');
        $this->closeForm();

    }

    public function edit($id)
    {
        $this->hydrate();
        $this->isUpdateMode = 'update';
        $data = Testimonials::where('id', $id)->first();

        $this->reviewId = $id;
        $this->name = $data->name;
        $this->position = $data->position;
        $this->review = $data->review;
    }


    public function update()
    {
        $this->validate();

        // only if validation get success
        $data = [
            'name' => $this->name,
            'position' => $this->position,
            'review' => $this->review,
        ];

        Testimonials::where('id', $this->reviewId)->update($data);
        session()->flash('message', 'Review successfully updated.');
        $this->closeForm();

    }

    public function delete($id)
    {
        if ($id) {
            Testimonials::where('id', $id)->delete();
            session()->flash('message', 'Review successfully deleted.');
        }
    }
}
