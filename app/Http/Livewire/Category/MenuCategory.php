<?php

namespace App\Http\Livewire\Category;

use App\Models\MenuItemCategory;
use Illuminate\Support\Str;
use Livewire\Component;

class MenuCategory extends Component
{
    public $category_id, $category, $menu, $data;
    public $menuData;

    protected $rules = [
        'category' => 'required|min:2',
        'menu' => 'required|min:2',
    ];

    public function resetFormFields()
    {
        $this->category = '';
    }

    // function is for removie all the error message and validation
    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function closeForm()
    {
        $this->resetFormFields();
        $this->hydrate();
    }

    public function store()
    {
        $this->validate();

        $data = [
            'name' => trim($this->category),
            'slug' => Str::slug($this->category),
            'main_menu' => $this->menu,
        ];

        MenuItemCategory::create($data);
        session()->flash('success', 'Menu category successfully added.');
        if ($this->menu == 'about') {
            return redirect()->route('about');
        } elseif ($this->menu == 'church') {
            return redirect()->route('ministries');
        } elseif ($this->menu == 'meeting') {
            return redirect()->route('meeting');
        } elseif ($this->menu == 'hgst') {
            return redirect()->route('hgst');
        } elseif ($this->menu == 'resource') {
            return redirect()->route('resources');
        }
    }

    public function mount($menuData)
    {
        $this->menu = $menuData;
    }

    public function render()
    {
        return view('livewire.category.menu-category');
    }
}
