<?php

namespace App\Http\Livewire\Category;

use App\Models\MenuItemCategory;
use Illuminate\Support\Str;
use Livewire\Component;

class UpdateMenuCategory extends Component
{
    public $category_id, $category, $menu, $data;
    public $menuData;

    protected $rules = [
        'category' => 'required|min:2',
        'menu' => 'required|min:2',
    ];

    public function resetFormFields()
    {
        $this->category = '';
    }

    // function is for removie all the error message and validation
    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function closeForm()
    {
        $this->resetFormFields();
        $this->hydrate();
    }

    public function mount($menuData, $categoryId)
    {
        $this->menu = $menuData;
        $this->category_id = $categoryId;
        $categoryData = MenuItemCategory::find($categoryId);
        $this->category = $categoryData->name;
    }

    public function update()
    {
        $this->validate();

        $data = [
            'name' => trim($this->category),
            'slug' => Str::slug($this->category),
        ];

        MenuItemCategory::where('id', $this->category_id)->update($data);

        session()->flash('success', 'Menu category successfully update.');
        if ($this->menu == 'about') {
            return redirect()->route('about');
        } elseif ($this->menu == 'church') {
            return redirect()->route('ministries');
        } elseif ($this->menu == 'meeting') {
            return redirect()->route('meeting');
        } elseif ($this->menu == 'hgst') {
            return redirect()->route('hgst');
        } elseif ($this->menu == 'resource') {
            return redirect()->route('resources');
        }
    }

    public function render()
    {
        $this->data = MenuItemCategory::find($this->category_id);
        return view('livewire.category.update-menu-category');
    }
}
