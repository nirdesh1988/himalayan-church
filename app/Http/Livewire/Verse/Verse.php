<?php

namespace App\Http\Livewire\Verse;

use Illuminate\Database\Eloquent\Model;
use Livewire\Component;
use App\Models\Verses;

class Verse extends Component
{
    public $page_title = 'verse of the day';
    public $data, $verseId, $verse, $reference, $display;

    public function resetFormFields()
    {
        $this->verse = '';
        $this->reference = '';
        $this->display = '';
    }

    // function is for removie all the error message and validation
    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function closeForm()
    {
        $this->emit('verseStore'); //TO CLOSE THE MODAL FORM. "verseStore" =>its a event name defined in the jquery in livewire component
        $this->emit('verseUpdate');
        $this->emit('verseDelete');
        $this->resetFormFields();
        $this->hydrate();
    }

    protected $rules = [
        'verse' => 'required|min:3',
        'reference' => 'required|min:3',
    ];

    protected $messages = [
        'verse.required' => 'Verse cannot be empty.',
        'verse.min' => 'Minimum 3 chars.',

        'reference.required' => 'Reference cannot be empty.',
        'reference.min' => 'Minimum 3 chars.',
    ];

    public function store()
    {
        $this->validate();

        // only if validation get success
        $data = new Verses([
            'verse' => $this->verse,
            'reference' => $this->reference,
        ]);
        $data->save();
        session()->flash('message', 'Verse of Day successfully saved.');
        $this->closeForm();
    }

    public function edit($id)
    {
        $this->verseId = $id;
        $data = Verses::where('id', $this->verseId)->first();
        $this->verse = $data->verse;
        $this->reference = $data->reference;
//        $this->display = $data->display;
    }


    public function update()
    {
        $this->validate();

        // only if validation get success
        $data = [
            'verse' => $this->verse,
            'reference' => $this->reference,
//            'display' => $this->display,
        ];

        Verses::where('id', $this->verseId)->update($data);
        session()->flash('message', 'Verse of Day successfully updated.');
        $this->closeForm();
    }

    public function delete($id)
    {
        $this->verseId = $id;
    }

    public function destroy()
    {
        if ($this->verseId) {
            Verses::where('id', $this->verseId)->delete();
            session()->flash('message', 'Verse of Day successfully deleted.');
            $this->closeForm();
        }
    }

    public function checkDisplayStatus($status, $verse_id)
    {
        $data = [
            'display' => $status
        ];

        if ($status == 'Y') {
            $countDisplay = Verses::where('display', 'Y')->get();
            if (count($countDisplay) >= 1) {
                return redirect()->back()->with('error', 'Max display limit is only 1.');
            } else {
                $result = Verses::where('id', $verse_id)->update($data);
                if ($result) {
                    return redirect()->back()->with('message', 'Verse of Day is managed to display...');
                }
            }
        } else {
            $result = Verses::where('id', $verse_id)->update($data);
            if ($result) {
                return redirect()->back()->with('message', 'Verse of Day is hidden to display...');
            }
        }

    }

    public function render()
    {
        $this->data = Verses::orderby('created_at', 'desc')->get();
        return view('livewire.verse.verse');
    }

}
