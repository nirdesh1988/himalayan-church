<li>
    <a href="">About Us</a>
    <ul class="dropdown">
        @foreach($menuCategory as $valMenuItem)
            @if($valMenuItem->main_menu=='about')
                <li>
                    <a href="{{ route('front.page.content',['slug'=> $valMenuItem->slug]) }}">{{ $valMenuItem->name }}</a>
                </li>
            @endif
        @endforeach
    </ul>
</li>
<li>
    <a href="">Church Ministries</a>
    <ul class="dropdown">
        @foreach($menuCategory as $valMenuItem)
            @if($valMenuItem->main_menu=='church')
                <li>
                    <a href="{{ route('front.page.content',['slug'=> $valMenuItem->slug]) }}">{{ $valMenuItem->name }}</a>
                </li>
            @endif
        @endforeach
    </ul>
</li>
<li>
    <a href="">Meeting Time</a>
    <ul class="dropdown">
        @foreach($menuCategory as $valMenuItem)
            @if($valMenuItem->main_menu=='meeting')
                <li>
                    <a href="{{ route('front.page.content',['slug'=> $valMenuItem->slug]) }}">{{ $valMenuItem->name }}</a>
                </li>
            @endif
        @endforeach
    </ul>
</li>
<li>
    <a href="">HGST</a>
    <ul class="dropdown">
        @foreach($menuCategory as $valMenuItem)
            @if($valMenuItem->main_menu=='hgst')
                <li>
                    <a href="{{ route('front.page.content',['slug'=> $valMenuItem->slug]) }}">{{ $valMenuItem->name }}</a>
                </li>
            @endif
        @endforeach
    </ul>
</li>
<li>
    <a class="" href="#">Resources</a>
    <ul class="dropdown">
        @foreach($menuCategory as $valMenuItem)
            @if($valMenuItem->main_menu=='resource')
                <li>
                    <a href="{{ route('front.resources.branch',['slug'=> $valMenuItem->slug]) }}">{{ $valMenuItem->name }}</a>
                </li>
            @endif
        @endforeach
    </ul>
</li>
<li><a href="{{ route('front.gallery') }}">Gallery</a>
</li>
<li><a href="{{ route('front.newsletter') }}">Newsletters</a>
</li>
<li><a href="{{ route('front.contact') }}">Contact</a>
</li>
