@props([
'data' => null,
'categoryId' => null,
])

<form wire:submit.prevent="update" id="frmStore">
    <div class="modal-body">
        <div class="form-group">
            <input type="hidden" readonly wire:model="category_id" class="form-control" value="{{ $categoryId }}">
            {{--            <input type="text" readonly class="form-control" value="{{ $data->name  }}">--}}
            @error('menu') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <label for="">Category <span class="text-danger">*</span></label>
            <input type="text" wire:model="category" class="form-control">
            @error('category') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </div>
</form>
