@props([
'categoryList' => null
])
<div class="block-content">
    <table class="table table-vcenter">
        <thead class="thead-light">
        <tr>
            <th class="text-center" style="width: 50px;">#</th>
            <th>Category</th>
            <th class="text-center" style="width: 100px;">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categoryList as $key => $valList)

            <tr>
                <th class="text-center" scope="row">{{ ++$key }}</th>
                <td class="font-w600 font-size-sm text-capitalize">
                    @if($valList->main_menu=='resource')
                        <a href="{{ route('resources.category.details',['slug'=>$valList->slug,'id'=>$valList->id])}}">{{ $valList->name }}</a>
                    @else
                        <a href="{{ route('menu.category.show',['menu'=>$valList->main_menu,'slug'=>$valList->slug,'id'=>$valList->id])}}">{{ $valList->name }}</a>
                    @endif
                </td>
                <td class="text-center">
                    <div class="btn-group">
                        <span data-toggle="tooltip" title="" data-original-title="Edit">
                            <a href="{{ route('menu.category.edit',['menu'=>$valList->main_menu,'id'=>$valList->id]) }}"
                               type="button"
                               class="btn btn-sm btn-light js-tooltip-enabled">
                                <i class="fa fa-edit text-success fa-font-icon-size"></i>
                            </a>
                        </span>
                        <span data-toggle="tooltip" title="" data-original-title="Remove">
                            <button data-id="{{ $valList->id }}" data-toggle="modal" data-target="#modalDeleteCategory"
                                    type="button" class="btn btn-sm btn-light js-tooltip-enabled">
                                <i class="fa fa-times text-danger fa-font-icon-size"></i>
                            </button>
                        </span>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <!-- Delete Gallery Modal -->
    <div class="modal fade" wire:ignore.self id="modalDeleteCategory" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Delete Menu Category</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" id="frmDelete">
                    @csrf
                    <div class="modal-body">
                        <p>Are you sure to delete this record?</p>
                        <input type="hidden" class="form-control" name="id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Yes, I confirm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@push('js_contents')
    <script>
        $('#modalDeleteCategory').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var categoryId = button.data('id') // Extract info from data-* attributes
            var modal = $(this)
            modal.find('.modal-body input').val(categoryId)
        })

        $('#frmDelete').submit(function (e) {
            e.preventDefault()

            $("#modalDeleteCategory").modal('hide')

            $.ajax({
                url: '{{ route('menu.category.delete') }}',
                data: new FormData(this),
                method: 'POST',
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data['success'] == true) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success...',
                            text: data['message'],
                            showConfirmButton: false,
                            timer: 1500
                        }).then(function () {
                            location.reload();
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: data['message'],
                            showConfirmButton: false,
                            timer: 1500
                        }).then(function () {
                            location.reload();
                        })
                    }
                }
            })
        })
    </script>
@endpush
