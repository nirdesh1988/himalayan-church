@props([
'menuData' => null,
])

<form wire:submit.prevent="store" id="frmStore">
    <div class="modal-body">
        <div class="form-group">
            <input type="hidden" readonly wire:model="menu" class="form-control" value="{{ $menuData }}">
            @error('menu') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <label for="">Category <span class="text-danger">*</span></label>
            <input type="text" wire:model="category" class="form-control">
            @error('category') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
</form>
