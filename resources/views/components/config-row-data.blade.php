@props([
'valData' => null,
])
<tr>
    <td class="font-w600 font-size-sm text-capitalize">{{ $valData->name }}</td>
    <td class="font-w600 font-size-sm">{{ $valData->value }}</td>
    <td class="text-center">
        <div class="btn-group">
            <span data-toggle="tooltip" title="" data-original-title="Edit">
                <button type="button" class="btn btn-sm btn-light js-tooltip-enabled">
                    <i class="fa fa-edit text-success fa-font-icon-size"></i>
                </button>
            </span>
        </div>
    </td>
</tr>
