<!-- Start Footer Section -->
<footer>

    <div class="container">
        <div class="row footer-widgets">
            <!-- Start Subscribe & Social Links Widget -->
            <div class="col-md-4 col-xs-12">
                <div class="footer-widget social-widget">
                    <h4>Follow Us<span class="head-line"></span></h4>
                    <p>Join our mailing list to stay up to date and get notices about our new releases!</p>
                    <br>
                    <ul class="social-icons">
                        @if(SystemConfigs::getConfig('facebook'))
                            <li><a class="facebook" href="{{ SystemConfigs::getConfig('facebook') }}"><i
                                        class="fa fa-facebook"></i></a></li>
                        @endif
                        @if(SystemConfigs::getConfig('instagram'))
                            <li><a class="instgram" href="{{ SystemConfigs::getConfig('instagram') }}"><i
                                        class="fa fa-instagram"></i></a></li>
                        @endif
                        @if(SystemConfigs::getConfig('twitter'))
                            <li><a class="twitter" href="{{ SystemConfigs::getConfig('twitter') }}"><i
                                        class="fa fa-twitter"></i></a></li>
                        @endif
                        @if(SystemConfigs::getConfig('google'))
                            <li><a class="google" href="{{ SystemConfigs::getConfig('google') }}"><i
                                        class="fa fa-google-plus"></i></a></li>
                        @endif
                        @if(SystemConfigs::getConfig('linkedin'))
                            <li><a class="linkdin" href="{{ SystemConfigs::getConfig('linkedin') }}"><i
                                        class="fa fa-linkedin"></i></a></li>
                        @endif
                        {{--                        @if(SystemConfig::getConfig('skype'))--}}
                        {{--                            <li><a class="skype" href="#"><i class="fa fa-skype"></i></a></li>--}}
                        {{--                        @endif--}}
                        @if(SystemConfigs::getConfig('pinterest'))
                            <li><a class="pinterest" href="{{ SystemConfigs::getConfig('pinterest') }}"><i
                                        class="fa fa-skype"></i></a></li>
                        @endif
                        @if(SystemConfigs::getConfig('youtube'))
                            <li><a class="youtube" href="{{ SystemConfigs::getConfig('youtube') }}"><i
                                        class="fa fa-youtube"></i></a></li>
                        @endif
                    </ul>
                </div>
            </div>


            <div class="col-md-4 col-xs-12">
                <div class="footer-widget flickr-widget">
                    <h4>Gallery<span class="head-line"></span></h4>
                    <ul class="flickr-list">
                        @foreach($footerGallery as $valGallery)
                            <li>
                                <a href="{{ asset('storage/'.$valGallery->image) }}" class="lightbox">
                                    <img alt="{{ $valGallery->title }}"
                                         src="{{ asset('storage/'.$valGallery->image) }}">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>


            <div class="col-md-4 col-xs-12">
                <div class="footer-widget contact-widget">
                    <h4><img src="{{ asset('images/whitelogo.png') }}" class="img-responsive" alt="Footer Logo"
                             style="width:60px;height:55px; margin-top:-18px"/></h4>
                    <ul>
                        <li><span>Address:</span> {{ SystemConfigs::getConfig('address')  }}</li>
                        <li><span>Phone Number:</span> {{ SystemConfigs::getConfig('contact')  }}</li>
                        <li><span>Email:</span> {{ SystemConfigs::getConfig('email')  }}</li>
                    </ul>
                </div>
            </div>

        </div>


        <!-- Start Copyright -->
        <div class="copyright-section">
            <div class="row">
                <div class="col-md-6">
                    <p>&copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script>
                        HIM - All Rights Reserved
                    </p>
                </div>
                <div class="col-md-6">
                    <ul class="footer-nav">
                        <li>
                            <p>For his glory, by the grace & made with
                                <svg width='15px' height='15px'
                                     xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-heart">
                                    <rect x="0" y="0" width="100" height="100" fill="none" class="bk">
                                    </rect>
                                    <g transform="translate(50 50)">
                                        <g>
                                            <g transform="translate(-50 -50)">
                                                <path
                                                    d="M90,30.2c0-11-9-20.1-20-20.2s-20,9.1-20,20.2c0,0.2,0-0.3,0,0.7H50c0-1,0-0.6,0-0.8c0-11-9-20.1-20-20.2s-20,9.1-20,20.2 c0,0.2,0-0.3,0,0.7h0c0.3,20,30,39.5,40,55c10-15.5,39.7-35,40-55h0C90,30,90,30.4,90,30.2z"
                                                    fill="#f02">
                                                </path>
                                            </g>
                                            <animateTransform
                                                attributeName="transform"
                                                type="scale"
                                                from="1.3" to="0.9" dur="2s"
                                                repeatCount="indefinite"
                                                calcMode="spline"
                                                values="1.3;0.9;1.1;0.9"
                                                keyTimes="0;0.3;0.301;1"
                                                keySplines="0 0.75 0.25 1;0 1 0 1;0 .75 .25 1">
                                            </animateTransform>
                                        </g>
                                    </g>
                                </svg>
                                :
                                <a href="http://www.maharjandipendra.com.np">Dipendra</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- .row -->
        </div>
        <!-- End Copyright -->

    </div>
</footer>
<!-- End Footer Section -->
