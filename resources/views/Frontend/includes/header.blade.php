<!doctype html>
<html lang="en">
<head>
    <title>Himalayan Impact Ministry | Himalan Evangelical Church</title>
    <link rel="shortcut icon" type="image/png" sizes="16x16" href="{{ asset('images/colorlogo.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('images/colorlogo.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="{{ asset('images/colorlogo.png') }}">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <meta name="author" content="Himalan Evangelical Church">
    <meta name="developer" content="Himalan Evangelical Church | devLuk Technologies | Iswor Lal Shrestha">
    <meta name="description" content="Himalan Evangelical Church"/>
    <meta name="keywords"
          content="himalayan church, nepal blood, blood bank, blood bank nepal, blood bank kathmandu, fresh blood, blood donation, blood donors, save life, blood donors kathmandu, blood donors nepal, online blood bank, online blood, donate blood, donating blood, smart blood management,common blood"/>

    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="3 month">
    <meta name="rating" content="General"/>
    <meta name="resource-type" content="document"/>
    <meta name="distribution" content="Global"/>
    <meta name="classification"
          content="himalayan church, nepal blood, blood bank, blood bank nepal, blood bank kathmandu, fresh blood, blood donation, blood donors, save life, blood donors kathmandu, blood donors nepal, online blood bank, online blood, donate blood, donating blood, smart blood management,common blood"/>

    <meta property="og:site_name" content="Himalan Evangelical Church"/>
    <meta property="og:type" content="article"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title"
          content="Himalan Evangelical Church | Blood Bank Kathmandu | Blood Bank Nepal | Blood Donors Nepal | Donate Blood - Save Life | Smart Blood Management | We work to encourage and inspire people to donate blood and provide fresh blood"/>
    <meta property="og:url" content="{{ route('front.home') }}"/>
    <meta property="og:description"
          content="Nepal Blood is a non-profitable service motive for the welfare of the society. We work to encourage and inspire people to donate blood to save life"/>
    <meta property="og:image" content="{{ asset('images/colorlogo.png') }}"/>

    <meta property="article:tag"
          content="nHimalan Evangelical Church, epal blood, blood bank, blood bank nepal, blood bank kathmandu, fresh blood, blood donation, blood donors, save life, blood donors kathmandu, blood donors nepal, online blood bank, online blood, donate blood, donating blood, smart blood management,common blood"/>
    <meta property="article:section"
          content="Himalan Evangelical Church | Blood Bank Kathmandu | Blood Bank Nepal | Blood Donors Nepal | Donate Blood - Save Life | Smart Blood Management | We work to encourage and inspire people to donate blood and provide fresh blood | Nepal Blood is a non-profitable service motive for the welfare of the society. We work to encourage and inspire people to donate blood to save life"/>

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" type="text/css" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slicknav.css') }}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/mystyle.css') }}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/blue.css') }}" title="blue" media="screen"/>

    @stack('styles')

</head>

<body>

<!-- Full Body Container -->
<div id="container">
