<!-- Start Header Section -->
<header class="clearfix">

    <!-- Start Top Bar -->
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <!-- Start Contact Info -->
                    <ul class="contact-details">
                        <li>
                            <a href="#">
                                <i class="fa fa-map-marker"></i> {{ SystemConfigs::getConfig('address')  }}
                            </a>
                        </li>
                        <li>
                            <a href="mailto:{{ SystemConfigs::getConfig('email')  }}">
                                <i class="fa fa-envelope-o"></i> {{ SystemConfigs::getConfig('email')  }}
                            </a>
                        </li>
                        <li>
                            <a href="tel:{{ SystemConfigs::getConfig('contact')  }}">
                                <i class="fa fa-phone"></i> {{ SystemConfigs::getConfig('contact')  }}
                            </a>
                        </li>
                    </ul>
                    <!-- End Contact Info -->
                </div>
                <!-- .col-md-6 -->

                <div class="col-md-6">
                    <!-- Start Social Links -->
                    <ul class="PEACE">
                        <li>
                            <a class="vision" data-placement="bottom">Our Vision</a>
                        </li>
                        <li>
                            <a class="p itl-tooltip" data-placement="bottom" title="Planting Model Churches"
                               href="#"><i class="fa">P</i></a>
                        </li>
                        <li>
                            <a class="e itl-tooltip" data-placement="bottom" title="Equipping Christian Leaders"
                               href="#"><i class="fa">E</i></a>
                        </li>
                        <li>
                            <a class="a itl-tooltip" data-placement="bottom"
                               title="Addressing Current Issues Biblically" href="#"><i class="fa">A</i></a>
                        </li>
                        <li>
                            <a class="c itl-tooltip" data-placement="bottom" title="Caring for Needy" href="#"><i
                                    class="fa">C</i></a>
                        </li>
                        <li>
                            <a class="e itl-tooltip" data-placement="bottom"
                               title="Educating Next Generations with Biblical Principles and Values" href="#"><i
                                    class="fa">E</i></a>
                        </li>
                    </ul>
                    <!-- End Social Links -->
                </div>
                <!-- .col-md-6 -->
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->
    </div>
    <!-- .top-bar -->
    <!-- End Top Bar -->

    <!-- Start  Logo & Naviagtion  -->
    <div class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <!-- Start Toggle Nav Link For Mobiles -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <!-- End Toggle Nav Link For Mobiles -->
                <a class="navbar-brand" href="{{ route('front.home') }}">
                    <img alt="Himalayan Impact Church" src="{{ asset('images/colorlogo.png') }}"
                         style="width:60px;height:55px; margin-top:-18px">
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <!-- Start Navigation List -->
                <ul class="nav navbar-nav navbar-right">
                    <x-menu-list-item :menuCategory="$menuCategory"/>
                </ul>
                <!-- End Navigation List -->
            </div>
        </div>

        <!--Mobile Menu Start-->
        <ul class="wpb-mobile-menu">
            <x-menu-list-item :menuCategory="$menuCategory"/>
        </ul>
        <!--Mobile Menu End-->
    </div>
    <!-- End Header Logo & Naviagtion -->

</header>
<!-- End Header Section -->
