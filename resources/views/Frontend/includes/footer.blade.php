</div>
<!-- End Full Body Container -->

<!-- Go To Top Link -->
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "url": "https://himalayanchurch.org",
  "name": "Himalan Evangelical Church",
  "contactPoint": {
    "@type": "PostalAddress",
		"addressCountry": "Nepal",
		"addressLocality": "Sanepa Heights",
		"addressRegion": "Bagmati",
		"streetAddress": "Lalitpur",
		"email": "info@himalayanchurch.org",
		"alternateName": "Himalan Evangelical Church",
		"description": "Himalayan Evanlegical Church is a non-profitable service motive circle of youths purely devoted for the welfare of the society.",
		"mainEntityOfPage": "https://himalayanchurch.org",
		"name": "Himalan Evangelical Church"
  }
}

</script>

<!-- JS  -->
<script type="text/javascript" src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.migrate.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/modernizrr.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fitvids.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/nivo-lightbox.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.isotope.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.appear.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/count-to.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.textillate.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.lettering.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.easypiechart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.nicescroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.parallax.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/mediaelement-and-player.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slicknav.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

@stack('scripts')

</body>

</html>
