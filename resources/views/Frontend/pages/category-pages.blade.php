@extends('Frontend.master')

@section('contents')


    <!-- Start Content -->
    <div class="mainbody">
        <div class="container">

            <!-- Page Heading/Breadcrumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header text-uppercase">{{ $menuItem->name }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="">
                                @if($menuItem->main_menu=='about')
                                    About Us
                                @elseif($menuItem->main_menu=='church')
                                    Church Ministries
                                @elseif($menuItem->main_menu=='meeting')
                                    Meeting Time
                                @elseif($menuItem->main_menu=='hgst')
                                    HGST
                                @elseif($menuItem->main_menu=='resource')
                                    Resources
                                @endif
                            </a>
                        </li>
                        <li class="active">{{ $menuItem->name }}</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <!-- Statement of faith Content -->
            <div class="row page-content-section">
                <div class="col-lg-12 sofimage">
                    @if($pageContent!=null)
                        <img class="img-responsive" src="{{ asset($pageContent->featured_image) }}"
                             alt="{{ $menuItem->name }}">
                    @endif
                </div>

                <div class="col-lg-12">
                    <div class="sofalign">
                        @if($pageContent!=null)
                            {!! $pageContent->contents !!}
                        @endif
                    </div>
                </div>
            </div>

            <div class="hr5" style="margin-top:30px; margin-bottom:45px;"></div>
            <!--Statement of faith end -->

        </div>
    </div>
    <!-- End Full Body Container -->

@endsection
