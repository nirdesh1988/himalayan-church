@extends('Frontend.master')

@section('contents')

    <!-- Start Content -->
    <div class="mainbody">
        <div class="container">
            <!-- Page Heading/Bread crumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header text-uppercase">{{ $menuItem->name }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="">
                                @if($menuItem->main_menu=='resource')
                                    Resources
                                @endif
                            </a>
                        </li>
                        <li class="active">{{ $menuItem->name }}</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="row page-content-section">
                @foreach($branchData as $valBranch)
                    <div class="col-md-3 col-sm-6 resource-page-section">
                        <div class="portfolio-item item">
                            <div class="portfolio-border">
                                <div class="portfolio-thumb">
                                    @if($valBranch->featured_image!='')
                                        @if(file_exists($valBranch->featured_image))
                                            <img alt="{{ $valBranch->title }}"
                                                 src="{{ asset($valBranch->featured_image) }}"/>
                                        @else
                                            <img alt="{{ $valBranch->title }}"
                                                 src="{{ asset('images/resources.png') }}"/>
                                        @endif
                                    @else
                                        <img alt="{{ $valBranch->title }}"
                                             src="{{ asset('images/resources.png') }}"/>
                                    @endif
                                </div>
                                <div class="portfolio-details">
                                    <a href="#"><p>{{ $valBranch->title }}</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="hr5" style="margin-top:50px; margin-bottom:55px;"></div>
        </div>
    </div>
    </div>
    <!-- End Full Body Container -->

@endsection
