<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>Online Enquiry: Himalayan Church</title>
    <style>
        body {
            background: #ededed;
            font-family: "Trebuchet MS";
            font-size: 16px;
        }

        .mail-panel {
            background: #fff;
            margin-top: 15px;
            padding: 0 10px
        }

        .mail-panel h2 {
            color: #e54750
        }

        .mail-panel h2 span {
            border-bottom: 2px solid #e54750 !important;
            padding-bottom: 12px
        }
    </style>
</head>
<body>

<div class="container mail-panel">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="padding:30px 0">
            <h2><span>Visitor's Enquiry : Himalayan Church</span></h2><br><br>
            <p>
                <strong>Name:</strong> {{ $data['name'] }} <br><br>
                <strong>Email:</strong> {{ $data['email'] }}<br><br>
                <strong>Subject:</strong> {{ $data['subject'] }}<br><br>
                <strong>Message:</strong> <br>{!! $data['message'] !!} <br><br>
                Thank You!
                <br><br><br><br><br><br>
                <i>This is auto genrated email. Please do not reply to this email</i>
            </p>
        </div>
    </div>
</div>

</body>
</html>
