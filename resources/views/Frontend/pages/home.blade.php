@extends('Frontend.master')

@section('contents')

    <!-- Start Home Page Slider -->
    <section id="home">
        <!-- Carousel -->
        <div id="main-slide" class="carousel slide" data-ride="carousel">

            <!-- Carousel inner -->
            <div class="carousel-inner">
                <div class="item active">
                    <img class="img-responsive" src="{{ asset('images/slider/bg1.jpg') }}" alt="slider">
                    <div class="slider-content">
                        <div class="col-md-12 text-center">
                            <h2 class="animated2">
                                <strong>Himalayan Impact Ministry</strong>
                            </h2>
                            <h3 class="animated3 white">
                                <span>Fear God, follow christ, find life.</span>
                            </h3>

                        </div>
                    </div>
                </div>
                <!--/ Carousel item end -->
                <div class="item">
                    <img class="img-responsive" src="{{ asset('images/slider/bg2.jpg') }}" alt="slider">
                    <div class="slider-content">
                        <div class="col-md-12 text-center">
                            <h2 class="animated4">
                                <strong>हिमालय प्रभाबकारी सेवकाई</strong>
                            </h2>
                            <h3 class="animated5 white">
                                <span>परिवर्तन होऊ र परिवर्तन ल्याऊ </span>
                            </h3>

                        </div>
                    </div>
                </div>
                <!--/ Carousel item end -->
                <div class="item">
                    <img class="img-responsive" src="{{ asset('images/slider/bg3.jpg') }}" alt="slider">
                    <div class="slider-content">
                        <div class="col-md-12 text-center">
                            <h2 class="animated7 white">
                                <strong>Compassion House</strong>
                            </h2>
                            <h3 class="animated8 white">
                                <span>House for everyone.</span>
                            </h3>

                        </div>
                    </div>
                </div>
                <!--/ Carousel item end -->
            </div>
            <!-- Carousel inner end-->

            <!-- Controls -->
            <a class="left carousel-control" href="#main-slide" data-slide="prev">
                <span><i class="fa fa-angle-left"></i></span>
            </a>
            <a class="right carousel-control" href="#main-slide" data-slide="next">
                <span><i class="fa fa-angle-right"></i></span>
            </a>
        </div>
        <!-- /carousel -->
    </section>
    <!-- End Home Page Slider -->

    <!-- Start Testimonials Section -->
    <div class="news">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <!-- Start Recent Posts Carousel -->
                    <div class="latest-posts">
                        <h4 class="classic-title"><span>Latest News</span></h4>
                        <div class="latest-posts-classic custom-carousel touch-carousel" data-appeared-items="2">
                            <!-- Start of Posts -->
                            @foreach($newsData as $valNews)
                                <div class="post-row item">
                                    <div class="left-meta-post">
                                        <div class="post-date">
                                            <span class="day">{{ $valNews->created_at->day }}</span>
                                            <span class="month">{{ $valNews->created_at->format('M') }}</span>
                                        </div>
                                        <div class="post-type"><i class="fa fa-picture-o"></i></div>
                                    </div>
                                    <h3 class="post-title text-uppercase"><a href="">{{ $valNews->title }}</a></h3>
                                    <div class="post-content">
                                        <p>
                                            {{ substr($valNews->short_info,0,100) }}
                                            <span><a class="read-more" href="">Read More...</a></span>
                                        </p>
                                    </div>
                                </div>
                        @endforeach
                        <!-- End of Posts -->
                        </div>
                    </div>
                    <!-- End Recent Posts Carousel -->
                </div>

                <div class="col-md-4">

                    <!-- Classic Heading -->
                    <h4 class="classic-title"><span>Testimonials</span></h4>

                    <!-- Start Testimonials Carousel -->
                    <div class="custom-carousel show-one-slide touch-carousel" data-appeared-items="1">
                        @foreach($testimonial as $valTestimonial)
                            <div class="classic-testimonials item">
                                <div class="testimonial-content">
                                    <p>
                                        {{ $valTestimonial->review }}
                                        <a class="read-more" href="">Read More...</a>
                                    </p>
                                </div>
                                <div class="testimonial-author">
                                    <span>{{ $valTestimonial->position }} - </span> {{ $valTestimonial->name }}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Testimonials Section -->

    <!-- Start Portfolio Section -->
    <div class="project">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <!-- Classic Heading -->
                    <h4 class="classic-title"><span>Video Sermons</span></h4>
                    <!-- Youtube video Iframe -->
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/nHvcqkd89xo?controls=0"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>

                </div>

                <div class="col-md-4 col-sm-6">
                    <h4 class="classic-title"><span>Audio Sermons</span></h4>
                    <a href="audio-sermons.php">
                        <img class="img-responsive" src="images/audio.jpg"/>
                    </a>
                </div>

                <div class="col-md-4 col-sm-6">
                    <h4 class="classic-title"><span>Articles</span></h4>
                    <a href="articles.php">
                        <img class="img-responsive" src="images/articles.jpg"/>
                    </a>
                </div>

            </div>
        </div>
        <!-- .container -->
    </div>
    <!-- End Portfolio Section -->

    <!-- Start bible Verses Section -->
    <div class="section purchase">
        <div class="container">

            <!-- Start Video Section Content -->
            <div class="section-video-content text-center">

                <!-- Start Animations Text -->
                <h1 class="fittext wite-text uppercase tlt">
                    <span class="texts">
                        <span>Bible Verse of the day</span>
                    </span>
                </h1><br/>
                <!-- End Animations Text -->
                <h4 class="wite-text">{{ $verse->verse }}</h4></br>
                <h4 class="wite-text">{{ $verse->reference }}</h4>
            </div>
            <!-- End Section Content -->

        </div>
        <!-- .container -->
    </div>
    <!-- End Bible verse Section -->

    <!-- Start Team Member Section -->
    <div class="section team-member-profile-section">
        <div class="container">
            <div id="leaders"></div>

            <!-- Start Big Heading -->
            <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
                <h1>Our Leaders</h1>
            </div>
            <!-- End Big Heading -->

            <!-- Some Text -->
            <p class="text-center">Our Church is equiped with quality leaders in different ministries.</p>


            <!-- Start Recent Projects Carousel -->
            <div class="recent-projects">
                <h4 class="title"><span>Our Church Leaders</span></h4>
                <div class="projects-carousel touch-carousel">

                    @foreach($leader as $valLeader)
                        <div class="portfolio-item item">
                            <div class="portfolio-border">
                                <div class="portfolio-thumb">
                                    <a class="lightbox" title="{{ $valLeader->information }}"
                                       href="{{ asset('storage/'.$valLeader->image) }}">
                                        <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                                        <img alt="{{ $valLeader->name }}"
                                             src="{{ asset('storage/'.$valLeader->image) }}"/>
                                    </a>
                                </div>
                                <div class="portfolio-details">
                                    <a>
                                        <h4 class="text-capitalize">{{ $valLeader->name }}</h4>
                                        <span class="text-capitalize">{{ $valLeader->position }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            <!-- End Recent Projects Carousel -->

        </div>
        <!-- .container -->
    </div>
    <!-- End Team Member Section -->

@endsection
