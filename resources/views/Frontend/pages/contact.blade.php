@extends('Frontend.master')

@push('styles')
    <style>
        .form-message-contact {
            font-size: 16px
        }
    </style>
@endpush

@section('contents')

    <!-- Start Content -->
    <div class="mainbody">

        <div class="container">
            <!-- Page Heading/Bread crumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Contact
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="">Home</a>
                        </li>
                        <li class="active">Contact</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <!-- Start Map -->
            <div style="width: 100%">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3532.96864847111!2d85.30521395714722!3d27.6873638539962!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb184f2a202221%3A0x843d501d08000a21!2sHimalayan+Evangelical+Church!5e0!3m2!1sen!2snp!4v1481364611652"
                    width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <!-- End Map -->

            <!-- Start Content -->
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            @if(session()->has('success'))
                                <div class="alert alert-success form-message-contact" id="displayMessage">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger form-message-contact" id="displayMessage">
                                    {{ session()->get('error') }}
                                </div>
                            @endif

                            <h4 class="classic-title"><span>Contact Us</span></h4>
                            <!-- Start Contact Form -->
                            <div class="contact-form" id="contact-form">
                                <form action="{{ route('front.contact.enquiry') }}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="text" placeholder="Name *" name="name"
                                                   value="{{ old('name') }}">
                                            @error('name')
                                            <span class="text-danger"
                                                  role="alert"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="email" class="email" placeholder="Email *" name="email"
                                                   value="{{ old('email') }}">
                                            @error('email')
                                            <span class="text-danger"
                                                  role="alert"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="text" class="requiredField" placeholder="Subject *"
                                                   name="subject" value="{{ old('subject') }}">
                                            @error('subject')
                                            <span class="text-danger"
                                                  role="alert"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <textarea rows="5" placeholder="Message *"
                                                      name="message">{{ old('message') }}</textarea>
                                            @error('message')
                                            <span class="text-danger"
                                                  role="alert"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <button type="submit" name="btnSubmit" id="submit" class="btn-system btn-large">
                                        Send
                                    </button>
                                    <div id="success" style="color:#34495e;"></div>
                                </form>
                            </div>
                            <!-- End Contact Form -->
                        </div>

                        <div class="col-md-4">
                            <h4 class="classic-title"><span>Information</span></h4>
                            <p>Himalayan Impact Ministry, Himalayan Evangelical Church and HGST</p>
                            <div class="hr1" style="margin-bottom:10px;"></div>
                            <ul class="icons-list">
                                <li>
                                    <i class="fa fa-map-marker"> </i> <strong>Address:</strong>
                                    {{ \App\Helpers\SystemConfig::getConfig('address') }}
                                </li>
                                <li>
                                    <i class="fa fa-envelope-o"></i> <strong>Email:</strong>
                                    {{ \App\Helpers\SystemConfig::getConfig('email') }}
                                </li>
                                <li>
                                    <i class="fa fa-envelope-o"></i> <strong>Email:</strong>
                                    info@himalayanchurch.org
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i> <strong>Phone:</strong>
                                    {{ \App\Helpers\SystemConfig::getConfig('contact') }}
                                </li>
                            </ul>
                            <div class="hr1" style="margin-bottom:15px;"></div>
                            <h4 class="classic-title"><span>Working Hours</span></h4>
                            <ul class="list-unstyled">
                                <li><strong>Monday - Friday</strong> - 9am to 5pm</li>
                                <li><strong>Saturday</strong> - 9am to 1pm</li>
                                <li><strong>Sunday</strong> - Closed</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End content -->

            <div class="hr5" style="margin-top:50px; margin-bottom:55px;"></div>
        </div>
    </div>
    <!-- End Full Body Container -->

@endsection
