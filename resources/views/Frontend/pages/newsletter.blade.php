@extends('Frontend.master')

@section('contents')


    <!-- Start Content -->
    <div class="mainbody">
        <div class="container">

            <!-- Page Heading/Breadcrumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Newsletter
                    </h2>
                    <ol class="breadcrumb">
                        <li><a href="">Home</a>
                        </li>
                        <li class="active">Newsletter</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->


            <div class="row blog-page">
                <!--Side bar-->
                <div class="col-md-3 sidebar left-sidebar">
                    <div class="widget widget-popular-posts">
                        <h4>Previous Newsletters <span class="head-line"></span></h4>
                        <ul>

                            <li>
                                <div class="widget-thumb">
                                    <a href="">
                                        <img
                                            src="https://www.alyth.org.uk/wp-content/uploads/2015/02/newsletter-icon.jpg"
                                            alt=""/>
                                    </a>
                                </div>
                                <div class="widget-content">
                                    <h5>
                                        <a href="">feb</a>
                                    </h5>
                                    <span>Posted on: 2012/12/25</span>
                                </div>
                                <div class="clearfix"></div>
                            </li>

                            <li>
                                <div class="widget-thumb">
                                    <a href="">
                                        <img
                                            src="https://www.alyth.org.uk/wp-content/uploads/2015/02/newsletter-icon.jpg"
                                            alt=""/>
                                    </a>
                                </div>
                                <div class="widget-content">
                                    <h5>
                                        <a href="">feb</a>
                                    </h5>
                                    <span>Posted on: 2012/12/25</span>
                                </div>
                                <div class="clearfix"></div>
                            </li>

                            <li>
                                <div class="widget-thumb">
                                    <a href="">
                                        <img
                                            src="https://www.alyth.org.uk/wp-content/uploads/2015/02/newsletter-icon.jpg"
                                            alt=""/>
                                    </a>
                                </div>
                                <div class="widget-content">
                                    <h5>
                                        <a href="">feb</a>
                                    </h5>
                                    <span>Posted on: 2012/12/25</span>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--End side bar-->

                <div class="col-md-9 service-box service-icon-left-more">
                    <div class="big-title">
                        <h1><span style="margin-left: 25px" class="accent-color">Newsletters</span></h1><br>

                        <div class="service-content col-md-12 mb-3">
                            <p class="title-desc"><strong>jan</strong></p>
                            <!-- Start Big Heading -->
                            <div class="col-md-3">
                                <img class="img-responsive"
                                     src="https://www.medvetforpets.com/wp-content/uploads/2016/10/newsletter-icon-e1480902772716.jpg"
                                     alt="">
                            </div>
                            <p>this iojdsalifu dsiakjf iudsaf </p>
                            <br>
                            <!-- Button Starts -->
                            <div class="button-side" style="margin-top:8px;" align="right">
                                <a href="" class="btn-system btn-small">Download</a>
                            </div>
                            <!-- Button Ends -->
                        </div>
                        <div class="service-content col-md-12">
                            <p class="title-desc"><strong>jan</strong></p>
                            <!-- Start Big Heading -->
                            <div class="col-md-3">
                                <img class="img-responsive"
                                     src="https://www.medvetforpets.com/wp-content/uploads/2016/10/newsletter-icon-e1480902772716.jpg"
                                     alt="">
                            </div>
                            <p>this iojdsalifu dsiakjf iudsaf </p>
                            <br>
                            <!-- Button Starts -->
                            <div class="button-side" style="margin-top:8px;" align="right">
                                <a href="" class="btn-system btn-small">Download</a>
                            </div>
                            <!-- Button Ends -->
                        </div>
                    </div>

                </div>
            </div>

            <div class="hr5" style="margin-top:30px; margin-bottom:45px;"></div>
            <!--Statement of faith end -->

        </div>
    </div>
    <!-- End Full Body Container -->

@endsection
