@extends('Frontend.master')

@section('contents')

    <!-- Start Content -->
    <div class="mainbody">
        <div class="container">
            <!-- Page Heading/Bread crumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Gallery
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="contact.php">Home</a>
                        </li>
                        <li class="active">Gallery</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                    <!-- Start Gallery -->
                    <div class="recent-projects gallery-section">
                        @foreach($galleryData as $valGallery)
                            <div class="col-md-3 col-sm-6">
                                <div class="portfolio-item item">
                                    <div class="portfolio-border">
                                        <div class="portfolio-thumb">
                                            <a class="lightbox" title="{{ $valGallery->title }}"
                                               href="{{ asset('storage/'.$valGallery->image) }}"
                                               data-lightbox-gallery="gallery1">
                                                <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                                                <img alt="{{ $valGallery->title }}"
                                                     src="{{ asset('storage/'.$valGallery->image) }}"/>
                                            </a>
                                        </div>
{{--                                        <div class="portfolio-details">--}}
{{--                                            <a href="#"><p>{{ $valGallery->title }}</p></a>--}}
{{--                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <!-- End Gallery -->
                </div>
            </div>
            <div class="hr5" style="margin-top:50px; margin-bottom:55px;"></div>
        </div>
    </div>
    </div>
    <!-- End Full Body Container -->

@endsection
