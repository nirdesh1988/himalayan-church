@extends('Frontend.master')

@section('contents')


    <!-- Start Content -->
    <div class="mainbody">
        <div class="container">

            <!-- Page Heading/Breadcrumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Statement of Faith
                    </h2>
                    <ol class="breadcrumb">
                        <li><a href="">About Us</a>
                        </li>
                        <li class="active">Statement of Faith</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <!-- Statement of faith Content -->
            <div class="row">
                <div class="col-lg-12 sofimage">
                    <img class="img-responsive" src="images/sof.jpg" alt="">
                </div>

                <div class="col-lg-12">
                    <div class="sofalign">
                        </br>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed voluptate nihil eum consectetur
                            similique? Consectetur, quod, incidunt, harum nisi dolores delectus reprehenderit voluptatem
                            perferendis dicta dolorem non blanditiis ex fugiat.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe, magni, aperiam vitae illum
                            voluptatum aut sequi impedit non velit ab ea pariatur sint quidem corporis eveniet. Odit,
                            temporibus reprehenderit dolorum!</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, consequuntur, modi mollitia
                            corporis ipsa voluptate corrupti eum ratione ex ea praesentium quibusdam? Aut, in eum facere
                            corrupti necessitatibus perspiciatis quis?</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed voluptate nihil eum consectetur
                            similique? Consectetur, quod, incidunt, harum nisi dolores delectus reprehenderit voluptatem
                            perferendis dicta dolorem non blanditiis ex fugiat.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe, magni, aperiam vitae illum
                            voluptatum aut sequi impedit non velit ab ea pariatur sint quidem corporis eveniet. Odit,
                            temporibus reprehenderit dolorum!</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, consequuntur, modi mollitia
                            corporis ipsa voluptate corrupti eum ratione ex ea praesentium quibusdam? Aut, in eum facere
                            corrupti necessitatibus perspiciatis quis?</p>
                    </div>
                </div>
            </div>

            <div class="hr5" style="margin-top:30px; margin-bottom:45px;"></div>
            <!--Statement of faith end -->

        </div>
    </div>
    <!-- End Full Body Container -->

@endsection
