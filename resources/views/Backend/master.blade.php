@include('Backend.includes.header')
@include('Backend.includes.nav-panel')
@include('Backend.includes.header-panel')

@yield('contents')

@include('Backend.includes.footer-panel')
@include('Backend.includes.footer')