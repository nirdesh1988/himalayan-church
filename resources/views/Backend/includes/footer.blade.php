<script src="{{ asset('assets/backend/assets/js/oneui.core.min.js') }}"></script>
<script src="{{ asset('assets/backend/assets/js/oneui.app.min.js') }}"></script>
<script src="{{ asset('assets/backend/assets/js/tagsinput.min.js') }}"></script>

{{--<script src="https://cdn.ckeditor.com/4.11.2/standard-all/ckeditor.js"></script>--}}

<!-- Page JS Plugins -->
{{--<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>--}}

<script src="{{ asset('assets/backend/assets/js/plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('assets/backend/assets/js/plugins/chart.js/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('assets/backend/assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/backend/assets/js/pages/be_forms_validation.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets/backend/assets/js/pages/be_pages_dashboard.min.js') }}"></script>
<script src="{{ asset('assets/backend/assets/js/plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

<script src="https://unpkg.com/react@16.8.6/umd/react.production.min.js"></script>
<script src="https://unpkg.com/react-dom@16.8.6/umd/react-dom.production.min.js"></script>
<script src="{{ asset('vendor/laraberg/js/laraberg.js') }}"></script>

<script src="{{ asset('assets/backend/assets/js/jquery.sweetalert.js') }}"></script>

{{--<script src="{{ asset('assets/backend/assets/js/custom.js') }}"></script>--}}
@livewireScripts

@stack('js_contents')

</body>
</html>
