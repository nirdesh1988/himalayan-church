<!-- Header -->
<header id="page-header">
    <!-- Header Content -->
    <div class="content-header">
        <!-- Left Section -->
        <div class="d-flex align-items-center">
            <!-- Toggle Sidebar -->
            <button type="button" class="btn btn-sm btn-dual mr-2 d-lg-none" data-toggle="layout"
                    data-action="sidebar_toggle">
                <i class="fa fa-fw fa-bars"></i>
            </button>
            <!-- END Toggle Sidebar -->

            <!-- Toggle Mini Sidebar -->
            <button type="button" class="btn btn-sm btn-dual mr-2 d-none d-lg-inline-block" data-toggle="layout"
                    data-action="sidebar_mini_toggle">
                <i class="fa fa-fw fa-ellipsis-v"></i>
            </button>
            <!-- END Toggle Mini Sidebar -->
        </div>
        <!-- END Left Section -->

    {{--        {{ dd(Auth::user()->userDetails) }}--}}

    <!-- Right Section -->
        <div class="d-flex align-items-center">
            <!-- User Dropdown -->
            <div class="dropdown d-inline-block ml-2">
                <button type="button" class="btn btn-sm btn-dual d-flex align-items-center"
                        id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    {{--<img class="rounded-circle"--}}
                         {{--@if(Auth::user()->userDetails->avatar!='')--}}
                         {{--src="{{ asset(Auth::user()->userDetails->avatar ) }}"--}}
                         {{--@else--}}
                         {{--src="{{ asset('assets/backend/assets/media/avatars/avatar13.jpg') }}"--}}
                         {{--@endif--}}
                         {{--alt="{{ Auth::user()->userDetails->name ? Auth::user()->userDetails->name:'Account Photo' }}"--}}
                         {{--width="25px">--}}
                    <?php
//                    $name = Auth::user()->userDetails->name;
//                    $firstName = explode(' ', trim($name));
                    ?>
                    {{--<span class="d-none d-sm-inline-block ml-2">{{ $firstName[0] }}</span>--}}
                    <i class="fa fa-fw fa-angle-down d-none d-sm-inline-block ml-1 mt-1"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-md dropdown-menu-right p-0 border-0"
                     aria-labelledby="page-header-user-dropdown">
                    {{--<div class="p-3 text-center bg-primary-dark rounded-top">--}}
                        {{--<img class="img-avatar img-avatar48 img-avatar-thumb"--}}
                             {{--@if(Auth::user()->userDetails->avatar!='')--}}
                             {{--src="{{ asset(Auth::user()->userDetails->avatar ) }}"--}}
                             {{--@else--}}
                             {{--src="{{ asset('assets/backend/assets/media/avatars/avatar13.jpg') }}"--}}
                             {{--@endif--}}
                             {{--alt="{{ Auth::user()->userDetails->name ? Auth::user()->userDetails->name:'Account Photo' }}">--}}
                        {{--<p class="mt-2 mb-0 text-white font-w500">{{ Auth::user()->userDetails->name }}</p>--}}
                        {{--<p class="mb-0 text-white-50 font-size-sm">{{ Auth::user()->userDetails->userRole->name }}</p>--}}
                    {{--</div>--}}
                    <div class="p-2">
                        {{--<a class="dropdown-item d-flex align-items-center justify-content-between"--}}
                           {{--href="{{ route('profile') }}">--}}
                            {{--<span class="font-size-sm font-w500">Profile</span>--}}
                        {{--</a>--}}
                        {{--<a class="dropdown-item d-flex align-items-center justify-content-between"--}}
                        {{--href="">--}}
                        {{--<span class="font-size-sm font-w500">Settings</span>--}}
                        {{--</a>--}}
                        <div role="separator" class="dropdown-divider"></div>

                        {{--<a class="dropdown-item d-flex align-items-center justify-content-between"--}}
                           {{--href="{{ route('admin.logout') }}">--}}
                            {{--<span class="font-size-sm font-w500">Log Out</span>--}}
                        {{--</a>--}}
                    </div>
                </div>
            </div>
            <!-- END User Dropdown -->
        </div>
        <!-- END Right Section -->
    </div>
    <!-- END Header Content -->

    <!-- Header Search -->
    <div id="page-header-search" class="overlay-header bg-white">
        <div class="content-header">
            <form class="w-100" action="be_pages_generic_search.html" method="POST">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <button type="button" class="btn btn-alt-danger" data-toggle="layout"
                                data-action="header_search_off">
                            <i class="fa fa-fw fa-times-circle"></i>
                        </button>
                    </div>
                    <input type="text" class="form-control" placeholder="Search or hit ESC.."
                           id="page-header-search-input" name="page-header-search-input">
                </div>
            </form>
        </div>
    </div>
    <!-- END Header Search -->

    <!-- Header Loader -->
    <!-- Please check out the Loaders page under Components category to see examples of showing/hiding it -->
    <div id="page-header-loader" class="overlay-header bg-white">
        <div class="content-header">
            <div class="w-100 text-center">
                <i class="fa fa-fw fa-circle-notch fa-spin"></i>
            </div>
        </div>
    </div>
    <!-- END Header Loader -->
</header>
<!-- END Header -->
