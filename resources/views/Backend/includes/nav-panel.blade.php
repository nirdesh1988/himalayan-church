<div id="page-container"
     class="sidebar-o sidebar-dark enable-page-overlay side-scroll page-header-fixed main-content-narrow">

    <nav id="sidebar" aria-label="Main Navigation">
        <!-- Side Header -->
        <div class="content-header bg-white-5">
            <!-- Logo -->
            <a class="font-w600 text-dual" href="{{ route('dashboard') }}">
                <span class="smini-visible"><i class="fa fa-circle-notch text-primary"></i></span>
                <span class="smini-hide font-size-h5 tracking-wider">
                    Himalayan<span class="font-w400"> Church</span>
                </span>
            </a>
            <!-- END Logo -->
            <div>
                <button type="button" class="d-lg-none ml-1 btn btn-dual btn-sm" data-toggle="layout"
                        data-action="sidebar_close">
                    <i class="fa fa-fw fa-times"></i>
                </button>
            </div>
        </div>
        <!-- END Side Header -->

        <!-- Sidebar Scrolling -->
        <div class="js-sidebar-scroll">
            <!-- Side Navigation -->
            <div class="content-side">
                <ul class="nav-main">
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ (request()->is('backend/system/dashboard','backend/system/dashboard/*')) ? 'active' : '' }}"
                           href="{{ route('dashboard') }}">
                            <i class="nav-main-link-icon fa fa-home"></i>
                            <span class="nav-main-link-name">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ (request()->is('backend/system/about','backend/system/about/*','backend/system/category/about','backend/system/category/about/*')) ? 'active' : '' }}"
                           href="{{ route('about') }}">
                            <i class="nav-main-link-icon fa fa-adn"></i>
                            <span class="nav-main-link-name">About</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ (request()->is('backend/system/church-ministries','backend/system/church-ministries/*','backend/system/category/church','backend/system/category/church/*')) ? 'active' : '' }}"
                           href="{{ route('ministries') }}">
                            <i class="nav-main-link-icon fa fa-fort-awesome"></i>
                            <span class="nav-main-link-name">Church Ministries</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ (request()->is('backend/system/meeting-time','backend/system/meeting-time/*','backend/system/category/meeting','backend/system/category/meeting/*')) ? 'active' : '' }}"
                           href="{{ route('meeting') }}">
                            <i class="nav-main-link-icon fa fa-linode"></i>
                            <span class="nav-main-link-name">Meeting Time</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ (request()->is('backend/system/hgst','backend/system/hgst/*','backend/system/category/hgst','backend/system/category/hgst/*')) ? 'active' : '' }}"
                           href="{{ route('hgst') }}">
                            <i class="nav-main-link-icon fa fa-linode"></i>
                            <span class="nav-main-link-name">HGST</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ (request()->is('backend/system/resources','backend/system/resources/*','backend/system/category/resource','backend/system/category/resource/*')) ? 'active' : '' }}"
                           href="{{ route('resources') }}">
                            <i class="nav-main-link-icon fa fa-linode"></i>
                            <span class="nav-main-link-name">Resources</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ (request()->is('backend/system/gallery','backend/system/gallery/*')) ? 'active' : '' }}"
                           href="{{ route('gallery') }}">
                            <i class="nav-main-link-icon fa fa-image"></i>
                            <span class="nav-main-link-name">Gallery</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ (request()->is('backend/system/verse','backend/system/verse/*')) ? 'active' : '' }}"
                           href="{{ route('verse') }}">
                            <i class="nav-main-link-icon fa fa-tag"></i>
                            <span class="nav-main-link-name">Verse of Day</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ (request()->is('backend/system/leader','backend/system/leader/*')) ? 'active' : '' }}"
                           href="{{ route('leader') }}">
                            <i class="nav-main-link-icon fa fa-user-secret"></i>
                            <span class="nav-main-link-name">Leaders</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ (request()->is('backend/system/news','backend/system/news/*')) ? 'active' : '' }}"
                           href="{{ route('news') }}">
                            <i class="nav-main-link-icon fa fa-newspaper"></i>
                            <span class="nav-main-link-name">News</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ (request()->is('backend/system/testimonials','backend/system/testimonials/*')) ? 'active' : '' }}"
                           href="{{ route('testimonials') }}">
                            <i class="nav-main-link-icon fa fa-comment"></i>
                            <span class="nav-main-link-name">Testimonials</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="{{ route('system.config') }}">
                            <i class="nav-main-link-icon fa fa-cogs"></i>
                            <span class="nav-main-link-name">System Configuration</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" target="_blank" href="{{ route('sitemap.generate') }}">
                            <i class="nav-main-link-icon fa fa-link"></i>
                            <span class="nav-main-link-name">Generate Sitemap</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="{{ route('db.backup') }}">
                            <i class="nav-main-link-icon fa fa-database"></i>
                            <span class="nav-main-link-name">Export DB Backup</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="{{ route('admin.logout') }}">
                            <i class="nav-main-link-icon fa fa-power-off text-danger"></i>
                            <span class="nav-main-link-name">Logout</span>
                        </a>
                    </li>

                </ul>
            </div>
            <!-- END Side Navigation -->
        </div>
        <!-- END Sidebar Scrolling -->
    </nav>
    <!-- END Sidebar -->
