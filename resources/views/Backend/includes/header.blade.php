<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Dashboard | Himalayan Church</title>
    <!-- csrf token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Icons -->
    <link rel="shortcut icon" href="{{ asset('images/colorlogo.png') }}" type="image/x-icon">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/backend/assets/css/oneui.min.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/backend/assets/css/customize.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/backend/assets/css/tagsinput.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    {{--<link rel="stylesheet" id="css-main" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="{{asset('vendor/laraberg/css/laraberg.css')}}">

    @livewireStyles
    @stack('css_contents')
</head>

<body>
