@extends('Backend.master')

@push('css_contents')
    <style>
        .alert {
            margin-bottom: 0
        }
    </style>
@endpush

@section('contents')
    <!-- Main Container -->
    <main id="main-container">

        <!-- Bread Crumb -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div
                    class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center py-2 text-center text-sm-left">
                    <div class="flex-sm-fill">
                        <h1 class="h3 font-w700 mb-2">{{ $menuData->name }} -
                            <small class="text-uppercase" style="font-size: 14px">{{ $data }}</small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Bread Crumb -->

        @if(session()->has('success'))
            <div class="block block-rounded">
                <div class="alert alert-success" id="displayMessage" style="padding: 25px">
                    {{ session()->get('success') }}
                </div>
            </div>

        @endif
        @if(session()->has('error'))
            <div class="block block-rounded">
                <div class="alert alert-danger" id="displayMessage" style="padding: 25px">
                    {{ session()->get('error') }}
                </div>
            </div>
    @endif

    <!-- Page Content -->
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="block block-rounded" style="padding: 25px">

{{--                        @if($data=='resource')--}}
{{--                            this is the resource section--}}
{{--                        @else--}}
                            <form method="post" action="{{ route('menu.category.update',['id'=> $menuData->id]) }}"
                                  enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" readonly value="{{ $menuData->id }}"
                                       class="form-control">
                                <div class="form-group">
                                <textarea id="content" name="contents"
                                          hidden>{!! $pageContent!=null? old('contents', $pageContent->contents):'' !!}</textarea>
                                    <span class="error_message text-danger" id="error_contents"></span>
                                </div>
                                <div class="form-group">
                                    <label for="">Featured Image</label>
                                    <input type="file" name="image" class="form-control">
                                    @if($pageContent!=null)
                                        @if($pageContent->featured_image!='')
                                            @if(file_exists($pageContent->featured_image))
                                                <br>
                                                <img src="{{ asset($pageContent->featured_image) }}"
                                                     alt="{{ $menuData->name }}"
                                                     width="150px">
                                            @endif
                                        @endif
                                    @endif
                                    <span class="error_message text-danger" id="error_image"></span>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>
{{--                        @endif--}}

                    </div>
                </div>

            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->

@endsection

@push('js_contents')
    <script>
        window.addEventListener('DOMContentLoaded', () => {
            Laraberg.init('content', {height: '100vh', laravelFilemanager: true})
        })

        {{--$('#frmUpdatePageContents').submit(function (e) {--}}
        {{--    e.preventDefault()--}}

        {{--    $.ajax({--}}
        {{--        url: '{{ route('menu.category.update') }}',--}}
        {{--        data: new FormData(this),--}}
        {{--        method: 'POST',--}}
        {{--        dataType: 'JSON',--}}
        {{--        contentType: false,--}}
        {{--        cache: false,--}}
        {{--        processData: false,--}}
        {{--        success: function (data) {--}}
        {{--            if (data['success'] == true) {--}}
        {{--                Swal.fire({--}}
        {{--                    icon: 'success',--}}
        {{--                    title: 'Success...',--}}
        {{--                    text: data['message'],--}}
        {{--                    showConfirmButton: false,--}}
        {{--                    timer: 1500--}}
        {{--                }).then(function () {--}}
        {{--                    location.reload();--}}
        {{--                })--}}
        {{--            } else {--}}
        {{--                Swal.fire({--}}
        {{--                    icon: 'error',--}}
        {{--                    title: 'Oops...',--}}
        {{--                    text: data['message'],--}}
        {{--                    showConfirmButton: false,--}}
        {{--                    timer: 1500--}}
        {{--                }).then(function () {--}}
        {{--                    location.reload();--}}
        {{--                })--}}
        {{--            }--}}
        {{--        },--}}
        {{--        error: function (response) {--}}
        {{--            var err_data = response['responseJSON']['errors'];--}}
        {{--            if (err_data) {--}}
        {{--                Object.keys(err_data).forEach(function (key) {--}}
        {{--                    $("#error_" + key).text(err_data[key]).show();--}}
        {{--                })--}}
        {{--            }--}}
        {{--        }--}}
        {{--    })--}}
        {{--})--}}
    </script>
@endpush
