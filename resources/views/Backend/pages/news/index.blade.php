@extends('Backend.master')

@section('css_contents')
@endsection

@section('contents')
    <!-- Main Container -->
    <main id="main-container">
        <!-- Bread Crumb -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center py-2 text-center text-sm-left">
                    <div class="flex-sm-fill">
                        {{--                        <h1 class="h3 font-w700 mb-2">{{ $page_title }}</h1>--}}
                    </div>
                </div>
            </div>
        </div>
        <!-- END Bread Crumb -->

        <!-- Page Content -->
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <livewire:news.news-events/>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
@endsection

@section('js_contents')
@endsection
