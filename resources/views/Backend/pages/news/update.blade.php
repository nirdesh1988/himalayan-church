@extends('Backend.master')

@section('css_contents')
@endsection

@section('contents')
    <!-- Main Container -->
    <main id="main-container">
        <!-- Bread Crumb -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div
                    class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center py-2 text-center text-sm-left">
                    <div class="flex-sm-fill">
                        {{--                        <h1 class="h3 font-w700 mb-2">{{ $page_title }}</h1>--}}
                    </div>
                </div>
            </div>
        </div>
        <!-- END Bread Crumb -->

        <!-- Page Content -->
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <div class="block block-rounded">
                            <div class="block-header">
                                <h3 class="block-title">{{ $page_title }}</h3>
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <span data-toggle="tooltip" title="" data-original-title="Back">
                                            <a href="{{ route('news') }}">
                                                <button class="btn btn-success"><i class="fa fa-reply"></i></button>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" id="displayMessage">
                                        {{ session()->get('success') }}
                                    </div>
                                @endif
                                @if(session()->has('error'))
                                    <div class="alert alert-danger" id="displayMessage">
                                        {{ session()->get('error') }}
                                    </div>
                                @endif
                                {{--                                                                <form id="frmUpdate" method="post" enctype="multipart/form-data">--}}
                                <form action="{{ route('news.events.update', ['id'=>$newsData->id]) }}" method="post"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" readonly name="id" class="form-control"
                                           value="{{ $newsData->id }}">
                                    <div class="form-group">
                                        <label for="">Title</label>
                                        <input type="text" name="title" class="form-control"
                                               value="{{ $newsData->title }}">
                                        <span class="error_message text-danger" id="error_title"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Short Info</label>
                                        <textarea name="short_info" rows="3" style="resize: none"
                                                  class="form-control">{{ $newsData->short_info }}</textarea>
                                        <span class="error_message text-danger" id="error_short_info"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Description</label>
                                        <textarea name="description" hidden
                                                  id="description">{!! $newsData->description !!}</textarea>
                                        <span class="error_message text-danger" id="error_description"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Image</label>
                                        <input type="file" name="image" class="form-control">
                                        @if(file_exists($newsData->image))
                                            <br>
                                            <img src="{{ asset($newsData->image) }}" alt="" width="120px">
                                        @endif

                                        <span class="error_message text-danger" id="error_image"></span>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary float-right">Update</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
@endsection

@push('js_contents')

    <script>
        window.addEventListener('DOMContentLoaded', () => {
            Laraberg.init('description', {height: '100vh', laravelFilemanager: true, sidebar: true})
        })

        {{--$('#frmUpdate').submit(function (e) {--}}
        {{--    e.preventDefault()--}}

        {{--    $.ajax({--}}
        {{--        url: '{{ route('news.events.update') }}',--}}
        {{--        data: new FormData(this),--}}
        {{--        method: 'POST',--}}
        {{--        dataType: 'JSON',--}}
        {{--        contentType: false,--}}
        {{--        cache: false,--}}
        {{--        processData: false,--}}
        {{--        success: function (data) {--}}
        {{--            if (data['success'] == true) {--}}
        {{--                Swal.fire({--}}
        {{--                    icon: 'success',--}}
        {{--                    title: 'Success...',--}}
        {{--                    text: data['message'],--}}
        {{--                    showConfirmButton: false,--}}
        {{--                    timer: 3000--}}
        {{--                }).then(function () {--}}
        {{--                    location.reload();--}}
        {{--                })--}}
        {{--            } else {--}}
        {{--                Swal.fire({--}}
        {{--                    icon: 'error',--}}
        {{--                    title: 'Oops...',--}}
        {{--                    text: data['message'],--}}
        {{--                    showConfirmButton: false,--}}
        {{--                    timer: 3000--}}
        {{--                }).then(function () {--}}
        {{--                    location.reload();--}}
        {{--                })--}}
        {{--            }--}}
        {{--        },--}}
        {{--        error: function (response) {--}}
        {{--            var err_data = response['responseJSON']['errors'];--}}
        {{--            if (err_data) {--}}
        {{--                Object.keys(err_data).forEach(function (key) {--}}
        {{--                    $("#error_" + key).text(err_data[key]).show();--}}
        {{--                })--}}
        {{--            }--}}
        {{--        }--}}
        {{--    })--}}
        {{--})--}}
    </script>

@endpush
