@extends('Backend.master')

@push('css_contents')
@endpush

@section('contents')
    <!-- Main Container -->
    <main id="main-container">

        <!-- Bread Crumb -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div {{--                        {{ dd(Auth::user()) }}--}}
                     class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center py-2 text-center text-sm-left">
                    <div class="flex-sm-fill">
                        <h1 class="h3 font-w700 mb-2">{{ $page_title }}</h1>

                        <h2 class="h6 font-w500 text-muted mb-0">
                            <?php
                            // dd(auth()->user()->userDetails);
                            //                            $name = Auth::user()->userDetails->name;
                            //                            $firstName = explode(' ', trim($name));
                            ?>
                            Welcome
                            {{--<a class="font-w600" href="javascript:void(0)">{{ $firstName[0] }}</a>--}}
                            , everything
                            looks great.
                        </h2>
                    </div>

                </div>
            </div>
        </div>
        <!-- END Bread Crumb -->

        <!-- Page Content -->
        <div class="content">
            <!-- Overview -->
        {{--<div class="row row-deck">--}}
        {{--<div class="col-sm-6 col-xl-3">--}}
        {{--<!-- Pending Orders -->--}}
        {{--<div class="block block-rounded d-flex flex-column">--}}
        {{--<div--}}
        {{--class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">--}}
        {{--<dl class="mb-0">--}}
        {{--<dt class="font-size-h2 font-w700">{{ $pendingOrder }}</dt>--}}
        {{--<dd class="text-muted mb-0">Pending Orders</dd>--}}
        {{--</dl>--}}
        {{--<div class="item item-rounded bg-body">--}}
        {{--<i class="fa fa-shopping-cart font-size-h3 text-primary"></i>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">--}}
        {{--<a class="font-w500 d-flex align-items-center" href="{{ route('orders') }}">--}}
        {{--View all orders--}}
        {{--<i class="fa fa-arrow-alt-circle-right ml-1 opacity-25 font-size-base"></i>--}}
        {{--</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- END Pending Orders -->--}}
        {{--</div>--}}
        {{--<div class="col-sm-6 col-xl-3">--}}
        {{--<!-- New Customers -->--}}
        {{--<div class="block block-rounded d-flex flex-column">--}}
        {{--<div--}}
        {{--class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">--}}
        {{--<dl class="mb-0">--}}
        {{--<dt class="font-size-h2 font-w700">{{ $activeCustomer }}</dt>--}}
        {{--<dd class="text-muted mb-0">Our Customers</dd>--}}
        {{--</dl>--}}
        {{--<div class="item item-rounded bg-body">--}}
        {{--<i class="fa fa-users font-size-h3 text-primary"></i>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">--}}
        {{--<a class="font-w500 d-flex align-items-center" href="{{ route('customers') }}">--}}
        {{--View all customers--}}
        {{--<i class="fa fa-arrow-alt-circle-right ml-1 opacity-25 font-size-base"></i>--}}
        {{--</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- END New Customers -->--}}
        {{--</div>--}}
        {{--<div class="col-sm-6 col-xl-3">--}}
        {{--<!-- Messages -->--}}
        {{--<div class="block block-rounded d-flex flex-column">--}}
        {{--<div--}}
        {{--class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">--}}
        {{--<dl class="mb-0">--}}
        {{--<dt class="font-size-h2 font-w700">{{ $activeChef }}</dt>--}}
        {{--<dd class="text-muted mb-0">Chefs</dd>--}}
        {{--</dl>--}}
        {{--<div class="item item-rounded bg-body">--}}
        {{--<i class="fa fa-utensils font-size-h3 text-primary"></i>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">--}}
        {{--<a class="font-w500 d-flex align-items-center" href="{{ route('chefs') }}">--}}
        {{--View all chefs--}}
        {{--<i class="fa fa-arrow-alt-circle-right ml-1 opacity-25 font-size-base"></i>--}}
        {{--</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- END Messages -->--}}
        {{--</div>--}}
        {{--<div class="col-sm-6 col-xl-3">--}}
        {{--<!-- Conversion Rate -->--}}
        {{--<div class="block block-rounded d-flex flex-column">--}}
        {{--<div--}}
        {{--class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">--}}
        {{--<dl class="mb-0">--}}
        {{--<dt class="font-size-h2 font-w700">{{ $activeFood }}</dt>--}}
        {{--<dd class="text-muted mb-0">Food Items</dd>--}}
        {{--</dl>--}}
        {{--<div class="item item-rounded bg-body">--}}
        {{--<i class="fa fa-hamburger font-size-h3 text-primary"></i>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">--}}
        {{--<a class="font-w500 d-flex align-items-center" href="{{ route('foods') }}">--}}
        {{--View all food items--}}
        {{--<i class="fa fa-arrow-alt-circle-right ml-1 opacity-25 font-size-base"></i>--}}
        {{--</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- END Conversion Rate-->--}}
        {{--</div>--}}
        {{--</div>--}}
        <!-- END Overview -->

            <!-- Statistics -->
            <div class="row">
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->

    <!-- START Delete Modal -->
    {{--    @include('Backend.includes.modal-delete',['prefixValueName'=>'orders', 'modalTitle'=>'Delete Order Details'])--}}
    <!-- END Delete Modal -->

@endsection

@push('js_contents')
@endpush
