@extends('Backend.master')

@push('css_contents')
@endpush

@section('contents')

    <!-- Main Container -->
    <main id="main-container">

        <!-- Bread Crumb -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div
                    class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center py-2 text-center text-sm-left">
                    <div class="flex-sm-fill">
                        {{--                        <h1 class="h3 font-w700 mb-2">{{ $page_title }}</h1>--}}
                    </div>
                </div>
            </div>
        </div>
        <!-- END Bread Crumb -->

        <!-- Page Content -->
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="block block-rounded">
                        <div class="block-header">
                            <h3 class="block-title">{{ $slug }} details
                                <a href="{{ route('resources.category.resources.add',['slug'=>$menuData->slug,'id'=>$menuData->id]) }}"
                                   style="margin-left: 10px" class="btn btn-primary block-title">add
                                    new {{ $slug }} branch</a>
                            </h3>
                            <div class="block-options">
                                <div class="block-options-item">
                                    <a href="{{ route('resources') }}" data-toggle="tooltip"
                                       data-placement="top" title="Back" class="btn btn-success block-title"><i
                                            class="fa fa-mail-reply"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="block-content">
                            <table class="table table-vcenter">
                                <thead class="thead-light">
                                <tr>
                                    <th class="text-center" style="width: 50px;">#</th>
                                    <th>Title</th>
                                    <th>Short Information</th>
                                    <th>Featured Image</th>
                                    <th class="text-center" style="width: 100px;">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($branchData as $key => $valBranch)
                                    <tr>
                                        <th class="text-center" scope="row">{{ ++$key }}</th>
                                        <td class="font-w600 font-size-sm">
                                            <a href="">{{ $valBranch->title }}</a>
                                        </td>
                                        <td class="font-w600 font-size-sm">{{ $valBranch->short_information }}</td>
                                        <td class="text-center">
                                            @if($valBranch->featured_image!='')
                                                @if(file_exists($valBranch->featured_image))
                                                    <img src="{{ asset($valBranch->featured_image) }}"
                                                         alt="{{ $valBranch->title }}" width="150">
                                                @endif
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                            <span data-toggle="tooltip" title="" data-original-title="Edit">
                                            <a href="{{ route('resources.category.resources.edit',['category'=>$slug,'slug'=>$valBranch->slug,'id'=>$valBranch->id]) }}"
                                               class="btn btn-sm btn-light js-tooltip-enabled">
                                                <i class="fa fa-edit text-success fa-font-icon-size"></i>
                                            </a>
                                            </span>

                                                <span data-toggle="tooltip" title="" data-original-title="Remove">
                                            <button type="button" class="btn btn-sm btn-light js-tooltip-enabled">
                                                <i class="fa fa-fw fa-times text-danger fa-font-icon-size"></i>
                                            </button>
                                            </span>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->

@endsection

@push('js_contents')
@endpush
