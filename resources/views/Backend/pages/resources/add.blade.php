@extends('Backend.master')

@push('css_contents')
@endpush

@section('contents')

    <!-- Main Container -->
    <main id="main-container">

        <!-- Bread Crumb -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div
                    class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center py-2 text-center text-sm-left">
                    <div class="flex-sm-fill">
                        {{--                        <h1 class="h3 font-w700 mb-2">{{ $page_title }}</h1>--}}
                    </div>
                </div>
            </div>
        </div>
        <!-- END Bread Crumb -->

        <!-- Page Content -->
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="block block-rounded">
                        <div class="block-header">
                            <h3 class="block-title">{{ $slug }} <i class="fa fa-long-arrow-right"></i> add branch</h3>
                            <div class="block-options">
                                <div class="block-options-item">
                                    <a href="{{ route('resources.category.details',['slug'=>$slug,'id'=>$id]) }}"
                                       data-toggle="tooltip"
                                       data-placement="top" title="Back" class="btn btn-success block-title">
                                        <i class="fa fa-mail-reply"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        @if(session()->has('success'))
                            <div class="col-md-12 mb-3">
                                <div class="alert alert-success" id="displayMessage">
                                    {{ session()->get('success') }}
                                </div>
                                <div class="clear-fix"></div>
                            </div>
                        @endif

                        <div class="block-content">

                            <form method="post" enctype="multipart/form-data"
                                  action="{{ route('resources.category.resources.store',['slug'=>$slug,'id'=>$id]) }}">
                                @csrf

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Title <span class="text-danger">*</span></label>
                                            <input type="text" name="title" class="form-control">
                                            @error('title') <span class="text-danger">{{ $message }}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Featured Image</label>
                                            <input type="file" name="image" class="form-control">
                                            <span class="error_message text-danger" id="error_image"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Short Information <span class="text-danger">*</span></label>
                                            <textarea type="text" name="short_info" rows="4" style="resize: none"
                                                      class="form-control"></textarea>
                                            @error('short_info') <span
                                                class="text-danger">{{ $message }}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea id="content_resource" name="contents" hidden></textarea>
                                            <span class="error_message text-danger" id="error_contents"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->

@endsection

@push('js_contents')
    <script>
        window.addEventListener('DOMContentLoaded', () => {
            Laraberg.init('content_resource', {height: '100vh', laravelFilemanager: true})
        })
    </script>
@endpush
