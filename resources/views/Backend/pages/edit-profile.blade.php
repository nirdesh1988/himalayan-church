@extends('Backend.master')

@push('css_contents')
    <link rel="stylesheet" href="{{ asset('css/cropper.css') }}">
@endpush

@section('contents')

    <style type="text/css">
        .img-container img#image {
            height: 500px;
            width: 100%;
            display: flex;
            object-fit: cover
        }

        img {
            display: block;
            max-width: 100%;
        }

        .preview {
            overflow: hidden;
            width: 120px;
            height: 120px;
            margin: 15px 0 0 0;
        }

        textarea {
            resize: none
        }
    </style>

    <!-- Main Container -->
    <main id="main-container">
        <!-- Hero -->
        <div class="bg-image"
             style="background-image: url(https://b.zmtcdn.com/data/pictures/7/19488637/d8d726d2a9ec2d3f3ee69a1b3447011c.jpg);">
            <div class="bg-black-75">
                <div class="content content-full text-center">
                    <div class="my-3">
                        <img class="img-avatar img-avatar-thumb"
                             @if(Auth::user()->userDetails->avatar!='')
                             src="{{ asset(Auth::user()->userDetails->avatar ) }}"
                             @else
                             src="{{ asset('assets/backend/assets/media/avatars/avatar13.jpg') }}"
                             @endif
                             alt="{{ Auth::user()->userDetails->name ? Auth::user()->userDetails->name:'Account Photo' }}">
                    </div>
                    <h1 class="h2 text-white mb-0">Update Profile Details</h1>
                    <h2 class="h4 font-w400 text-white-75">{{ Auth::user()->userDetails->name  }}</h2>
                    <a class="btn btn-light" href="{{ route('profile') }}">
                        <i class="fa fa-fw fa-arrow-left text-danger"></i> Back to Profile
                    </a>
                </div>
            </div>
        </div>
        <!-- END Hero -->

        <!-- Page Content -->
        <div class="content content-boxed">
            <!-- Avatar Profile -->
            <div class="block block-rounded">
                <div class="block-header">
                    <h3 class="block-title">Update your Avatar</h3>
                </div>
                <div class="block-content">
                    {{--<form action="" method="POST" enctype="multipart/form-data">--}}
                    <div class="row push">
                        <div class="col-lg-4">
                            <p class="font-size-sm text-muted">
                                Your account’s profile picture.
                            </p>
                        </div>
                        <div class="col-lg-8 col-xl-5">

                            <div class="picture-section">
                                <img class="img-avatar"
                                     @if(Auth::user()->userDetails->avatar!='')
                                     src="{{ asset(Auth::user()->userDetails->avatar ) }}"
                                     @else
                                     src="{{ asset('assets/backend/assets/media/avatars/avatar13.jpg') }}"
                                     @endif
                                     alt="{{ Auth::user()->userDetails->name ? Auth::user()->userDetails->name:'Account Photo' }}">

                                <div class="border-gray-600 text-gray-700 delete-account bg-gray-100 btn transition">
                                    <label class="btn btn-primary" for="account-upload">Update account photo</label>
                                    <input type="file" id="account-upload" hidden="" name="image" class="image">
                                </div>
                                <p style="margin-left: 85px">
                                    <small>Please ensure your photo is not larger than 2 MB.</small>
                                </p>
                            </div>
                        </div>
                    </div>
                    {{--</form>--}}
                </div>
            </div>
            <!-- END Avatar Profile -->

            <!-- Short Information -->
            <div class="block block-rounded">
                <div class="block-header">
                    <h3 class="block-title">Update your short introduction</h3>
                </div>
                <div class="block-content">
                    <form id="frmUpdateInformation" method="POST">
                        <div class="row push">
                            <div class="col-lg-4">
                                <p class="font-size-sm text-muted">
                                    Your short information
                                </p>
                            </div>
                            <div class="col-lg-8 col-xl-5">
                                <div class="form-group">
                                    <textarea type="text" class="form-control" rows="4" name="short_info"
                                              data-validation="required">{{ $data->short_info }}</textarea>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="Update Introduction">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Short Information -->

            <!-- User Profile -->
            <div class="block block-rounded">
                <div class="block-header">
                    <h3 class="block-title">Update Your Profile</h3>
                </div>
                <div class="block-content">
                    {{--<form method="post" action="{{ route('profile.update') }}">--}}
                    {{--@csrf--}}
                    {{--<form id="frmProfileUpdate" action="/my-profile/update">--}}
                    <form id="frmProfileUpdate" method="post">
                        @csrf
                        <div class="row push">
                            <div class="col-lg-4">
                                <p class="font-size-sm text-muted">
                                    Your account’s vital info. Your username will be publicly visible.
                                </p>
                            </div>
                            <div class="col-lg-8 col-xl-5">
                                <div class="form-group">
                                    <label for="val-username">Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="val-name" name="name"
                                           placeholder="Enter a Full Name.." value="{{ $data->name }}"
                                           data-validation="required">
                                    <span id="error_name"></span>
                                </div>
                                <div class="form-group">
                                    <label for="one-profile-edit-name">Address</label>
                                    <input type="text" class="form-control" id="val-address" name="address"
                                           placeholder="Enter your Address.." value="{{ $data->address }}"
                                           data-validation="required">
                                    <span id="error_address"></span>
                                </div>
                                <div class="form-group">
                                    <label for="val-email">Email <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="val-email" name="email" readonly=""
                                           placeholder="Enter your valid email.." value="{{ $data->email }}"
                                           data-validation="required">
                                </div>
                                <div class="form-group">
                                    <label for="val-email">Contact <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control numericOnly" id="val-contact" name="contact"
                                           placeholder="Enter your mobile number.." value="{{ $data->contact }}"
                                           data-validation="required|max:10|min:10">
                                    <span id="error_contact"></span>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="Update Profile">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END User Profile -->

            <!-- Change Password -->
            <div class="block block-rounded">
                <div class="block-header">
                    <h3 class="block-title">Change Password</h3>
                </div>
                <div class="block-content">
                    <form id="frmProfilePasswordUpdate" action="" method="POST">
                        @csrf
                        <div class="row push">
                            <div class="col-lg-4">
                                <p class="font-size-sm text-muted">
                                    Changing your sign in password is an easy way to keep your account secure.
                                </p>
                            </div>
                            <div class="col-lg-8 col-xl-5">
                                <div class="form-group">
                                    <label for="val-password">Current Password <span
                                            class="text-danger">*</span></label>
                                    <input type="password" class="form-control" id="val-old-password"
                                           name="old_password" placeholder="Provide current password...">
                                    <span id="error_old_password"></span>
                                </div>
                                <div class="form-group">
                                    <label for="val-password">Password <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" id="val-password" name="new_password"
                                           placeholder="Choose a safe one...">
                                    <span id="error_new_password"></span>
                                </div>
                                <div class="form-group">
                                    <label for="val-confirm-password">Confirm Password <span
                                            class="text-danger">*</span></label>
                                    <input type="password" class="form-control" id="val-confirm-password"
                                           name="confirm_password" placeholder="...and confirm it!">
                                    <span id="error_confirm_password"></span>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="Update Password">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Change Password -->
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->


    <!--picture modal-->
    <div class="modal fade modal-custom" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabel">Add a new account photo</h4>
                    <button type="button" class="close bg-green-500" data-dismiss="modal" aria-label="Close">
                        <span class="lnr lnr-cross"></span>
                    </button>
                </div>
                <form method="post">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="img-container">
                            <div class="display-flex mb-3">
                                <div>
                                    <img id="image" src="">
                                </div>
                                <div>
                                    <div class="preview" id="mycanvas"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer display-flex display-flex-container">
                        <div class="display-flex-child">
                            <button type="button" id="btnCancel" class="btn btn-alt-warning" data-dismiss="modal">Cancel
                            </button>
                        </div>
                        <div class="display-flex-child">
                            <input type="submit" id="cropUpload" data-dismiss="modal" class="btn btn-alt-primary"
                                   value="Upload">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end picture modal-->

@endsection

@push('js_contents')
    <script src="{{ asset('js/cropper.js') }}"></script>

    <script>
        //        avatar croper section
        var $modal = $('#modalUpload');
        var image = document.getElementById('image');
        var cropper;

        $("body").on("change", ".image", function (e) {
            var files = e.target.files;
            var done = function (url) {
                image.src = url;
                $modal.modal('show');
            };
            var reader;
            var file;
            var url;

            if (files && files.length > 0) {
                file = files[0];

                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function (e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });

        $modal.on('shown.bs.modal', function () {
            cropper = new Cropper(image, {
                aspectRatio: 1,
                viewMode: 2,
                preview: '.preview'
            });
        }).on('hidden.bs.modal', function () {
            cropper.destroy();
            cropper = null;
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#cropUpload").click(function () {
            canvas = cropper.getCroppedCanvas({
                width: 820,
                height: 620,
            });

            var canvasImage = document.getElementById("mycanvas");
            var imageCanvas = canvas.toDataURL("image/png");

            canvas.toBlob(function (blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    var base64data = reader.result;
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "{{ route('profile.avatar.update') }}",
                        data: {image: base64data},
                        success: function (data) {
                            if (data['success'] == true) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Success...',
                                    text: data['message'],
                                    showConfirmButton: false,
                                    timer: 3000
                                }).then(function () {
                                    location.reload();
                                })
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Opps...',
                                    text: data['message'],
                                    showConfirmButton: false,
                                    timer: 3000
                                })
                            }
                        }
                    })
                }
            })
        })

        $("#btnCancel").click(function () {
            location.reload()
        })
        $('#modalUpload').on('hidden.bs.modal', function () {
            location.reload()
        })


        // profile update section
        $("#frmProfileUpdate").submit(function (e) {
            e.preventDefault()

            $.ajax({
                url: '{{ route('profile.update') }}',
                method: 'post',
                data: $(this).serialize(),
                success: function (data) {
                    if (data['success'] == true) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success...',
                            text: data['message'],
                            showConfirmButton: false,
                            timer: 3000
                        }).then(function () {
                            location.reload();
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: data['message'],
                            showConfirmButton: false,
                            timer: 3000
                        }).then(function () {
                            location.reload();
                        })
                    }

                }
                ,
                error: function (response) {
                    var err_data = response['responseJSON']['errors'];
                    if (err_data) {
                        Object.keys(err_data).forEach(function (key) {
                            $("#error_" + key).text(err_data[key]).css('color', 'red').show();
                        })
                    }
                }
            })
        })

        // profile password update section
        $("#frmProfilePasswordUpdate").submit(function (e) {
            e.preventDefault()

            $.ajax({
                url: '{{ route('profile.update.password') }}',
                method: 'post',
                data: $(this).serialize(),
                success: function (data) {
                    if (data['success'] == true) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success...',
                            text: data['message'],
                            showConfirmButton: false,
                            timer: 3000
                        }).then(function () {
                            location.reload();
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: data['message'],
                            showConfirmButton: false,
                            timer: 3000
                        }).then(function () {
                            location.reload();
                        })
                    }

                },
                error: function (response) {
                    var err_data = response['responseJSON']['errors'];
                    if (err_data) {
                        Object.keys(err_data).forEach(function (key) {
                            $("#error_" + key).text(err_data[key]).css('color', 'red').show();
                        })
                    }
                }
            })
        })

        // profile introduction update section
        $("#frmUpdateInformation").submit(function (e) {
            e.preventDefault()

            $.ajax({
                url: '{{ route('profile.update.introduction') }}',
                method: 'post',
                data: $(this).serialize(),
                success: function (data) {
                    if (data['success'] == true) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success...',
                            text: data['message'],
                            showConfirmButton: false,
                            timer: 3000
                        }).then(function () {
                            location.reload();
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: data['message'],
                            showConfirmButton: false,
                            timer: 3000
                        }).then(function () {
                            location.reload();
                        })
                    }

                },
                error: function (response) {
                    var err_data = response['responseJSON']['errors'];
                    if (err_data) {
                        Object.keys(err_data).forEach(function (key) {
                            $("#error_" + key).text(err_data[key]).css('color', 'red').show();
                        })
                    }
                }
            })
        })

        $("input").focus(function () {
            $(this).next("span").html('');
        });

    </script>

@endpush
