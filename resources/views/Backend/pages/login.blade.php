<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Login System | Himalayan Evangelical Church </title>
    <link rel="shortcut icon" type="image/png" sizes="16x16" href="{{ asset('images/colorlogo.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('images/colorlogo.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="{{ asset('images/colorlogo.png') }}">
    <!-- Stylesheets -->
    <!-- Fonts and OneUI framework -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/backend/assets/css/oneui.min.css') }}">
    <!-- END Stylesheets -->
</head>
<body>

<div id="page-container">

    <!-- Main Container -->
    <main id="main-container">

        <!-- Page Content -->
        <div class="hero-static d-flex align-items-center">
            <div class="w-100">
                <!-- Sign In Section -->
                <div class="bg-white">
                    <div class="content content-full">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-lg-6 col-xl-4 py-4">
                                <!-- Header -->
                                <div class="text-center">
                                    <p class="mb-2">
                                        <i class="fa fa-2x fa-spin fa-circle-notch text-primary"></i>
                                    </p>
                                    <h1 class="h4 mb-1">
                                        Sign In
                                    </h1>
                                    <h2 class="h6 font-w400 text-muted mb-3">
                                        Himalayan Church : Backend System Panel
                                    </h2>

                                    @if(session()->has('error'))
                                        <div class="alert alert-danger text-center displayMessage">
                                            {{ session()->get('error') }}
                                        </div>
                                    @endif
                                </div>
                                <!-- END Header -->

                                <!-- Sign In Form -->
                                <form class="js-validation-signin" action="{{ route('admin.login.submit') }}"
                                      method="POST">
                                    @csrf

                                    <div class="py-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-lg form-control-alt"
                                                   id="login-username" name="login-username" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-lg form-control-alt"
                                                   id="login-password" name="login-password" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <div class="d-md-flex align-items-md-center justify-content-md-between">
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="login-remember" name="login-remember">
                                                    <label class="custom-control-label font-w400" for="login-remember">Remember
                                                        Me</label>
                                                </div>
                                                <div class="py-2">
                                                    <a class="font-size-sm font-w500"
                                                       href="{{ route('admin.forget.password') }}">Forgot
                                                        Password?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row justify-content-center mb-0">
                                        <div class="col-md-6 col-xl-5">
                                            <button type="submit" class="btn btn-block btn-primary">
                                                <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Sign In
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!-- END Sign In Form -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Sign In Section -->

                <!-- Footer -->
                <div class="font-size-sm text-center text-muted py-3">
                    <strong>Himalayan Evangelical Church - Lalitpur, Nepal</strong> &copy; <span
                        data-toggle="year-copy"></span>
                </div>
                <!-- END Footer -->
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
</div>
<!-- END Page Container -->

<script src="{{ asset('assets/backend/assets/js/oneui.core.min.js') }}"></script>
<script src="{{ asset('assets/backend/assets/js/oneui.app.min.js') }}"></script>
<!-- Page JS Plugins -->
<script src="{{ asset('assets/backend/assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<!-- Page JS Code -->
<script src="{{ asset('assets/backend/assets/js/pages/op_auth_signin.min.js') }}"></script>
<script>
    setTimeout("$('.displayMessage').fadeOut();", 2500);
</script>
</body>
</html>
