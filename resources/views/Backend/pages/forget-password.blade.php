<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Forget Password | Himalayan Evangelical Church </title>
    <link rel="shortcut icon" type="image/png" sizes="16x16" href="{{ asset('images/colorlogo.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('images/colorlogo.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="{{ asset('images/colorlogo.png') }}">
    <!-- Stylesheets -->
    <!-- Fonts and OneUI framework -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/backend/assets/css/oneui.min.css') }}">
    <!-- END Stylesheets -->
</head>
<body>

<div id="page-container">
    <!-- Main Container -->
    <main id="main-container">

        <!-- Page Content -->
        <div class="hero-static d-flex align-items-center">
            <div class="w-100">
                <!-- Reminder Section -->
                <div class="bg-white">
                    <div class="content content-full">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-lg-6 col-xl-4 py-4">
                                <!-- Header -->
                                <div class="text-center">
                                    <p class="mb-2">
                                        <i class="fa fa-2x fa-spin fa-circle-notch text-primary"></i>
                                    </p>
                                    <h1 class="h4 mb-1">
                                        Password Recover
                                    </h1>
                                    <h2 class="h6 font-w400 text-muted mb-3">
                                        Please provide your account’s email and we will send you your password.
                                    </h2>
                                </div>
                                <!-- END Header -->

                                <!-- Reminder Form -->
                                <form class="js-validation-reminder" action="" method="POST"
                                      novalidate="novalidate">
                                    <div class="form-group py-3">
                                        <input type="email" class="form-control form-control-lg form-control-alt"
                                               id="reminder-credential" name="reminder-credential"
                                               placeholder="Email">
                                    </div>
                                    <div class="form-group row justify-content-center">
                                        <div class="col-md-7">
                                            <button type="submit" class="btn btn-block btn-primary">
                                                <i class="fa fa-fw fa-envelope mr-1"></i> Recover Password
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!-- END Reminder Form -->

                                <div class="text-center">
                                    <a class="font-size-sm font-w500" href="{{ route('admin.login') }}">Login?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Reminder Section -->

                <!-- Footer -->
                <div class="font-size-sm text-center text-muted py-3">
                    <strong>Himalayan Evangelical Church - Lalitpur, Nepal</strong> &copy; <span
                        data-toggle="year-copy"></span>
                </div>
                <!-- END Footer -->
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
</div>
<!-- END Page Container -->

<script src="{{ asset('assets/backend/assets/js/oneui.core.min.js') }}"></script>
<script src="{{ asset('assets/backend/assets/js/oneui.app.min.js') }}"></script>
<!-- Page JS Plugins -->
<script src="{{ asset('assets/backend/assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<!-- Page JS Code -->
{{--<script src="{{ asset('assets/backend/assets/js/pages/op_auth_signin.min.js') }}"></script>--}}
<script src="{{ asset('assets/backend/assets/js/pages/op_auth_reminder.min.js') }}"></script>

</body>
</html>
