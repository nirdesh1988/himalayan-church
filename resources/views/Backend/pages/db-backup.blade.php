@extends('Backend.master')

@push('css_contents')
@endpush

@section('contents')
    <!-- Main Container -->
    <main id="main-container">

        <!-- Bread Crumb -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div
                    class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center py-2 text-center text-sm-left">
                    <div class="flex-sm-fill">
                    </div>
                </div>
            </div>
        </div>
        <!-- END Bread Crumb -->

        <!-- Page Content -->
        <div class="content">

            <div class="block block-rounded">
                <div class="block-header">
                    <h3 class="block-title">{{ $page_title }}</h3>
                    <div class="block-options">
                        <div class="block-options-item">
                            <a href="{{ route('db.backup.generate') }}" class="btn btn-primary block-title">
                                Export backup
                            </a>
                        </div>
                    </div>
                </div>

                <div class="block-content">
                    @if(session()->has('message'))
                        <div class="alert alert-success" id="displayMessage">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="clearfix"></div>
                    <table class="table table-vcenter gallery-section">
                        <thead class="thead-light">
                        <tr>
                            <th class="text-center" style="width: 50px;">#</th>
                            <th>Image</th>
                            <th>Backup Date</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $valData)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td><span class="alert alert-primary">{{ $valData->file_name }}</span></td>
                                <td>{{ $valData->created_at }}</td>
                                <td>
                                    <a href="{{ route('db.backup.dowload',['filename'=>$valData->file_name ]) }}"
                                       class="btn btn-warning text-uppercase"
                                       data-toggle="tooltip" data-placement="top"
                                       title="Download DB Backup">download</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->


@endsection

@push('js_contents')
@endpush
