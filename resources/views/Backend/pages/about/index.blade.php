@extends('Backend.master')

@push('css_contents')
@endpush

@section('contents')
    <!-- Main Container -->
    <main id="main-container">

        <!-- Bread Crumb -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div
                    class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center py-2 text-center text-sm-left">
                    <div class="flex-sm-fill">
                        {{--                        <h1 class="h3 font-w700 mb-2">{{ $page_title }}</h1>--}}
                    </div>
                </div>
            </div>
        </div>
        <!-- END Bread Crumb -->

        <!-- Page Content -->
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="block block-rounded">
                        <div class="block-header">
                            <h3 class="block-title">{{ $page_title }}</h3>
                            <div class="block-options">
                                <div class="block-options-item">
                                    <a href="{{ route('menu.category',['menu'=>'about']) }}"
                                       class="btn btn-primary block-title">add about category
                                    </a>
                                </div>
                            </div>
                        </div>

                        @if(session()->has('success'))
                            <div class="col-md-6 mb-3">
                                <div class="alert alert-success" id="displayMessage">
                                    {{ session()->get('success') }}
                                </div>
                                <div class="clear-fix"></div>
                            </div>
                        @endif

                        <x-menu-category-list :categoryList="$data"/>

                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->


@endsection

@push('js_contents')
@endpush
