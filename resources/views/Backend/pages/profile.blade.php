@extends('Backend.master')

@push('css_contents')
@endpush

@section('contents')
    <style>
        .profile-gallery-section img {
            width: 100%;
            height: 175px;
            object-fit: cover;
            display: flex
        }
    </style>
    <!-- Main Container -->
    <main id="main-container">

        <!-- Hero -->
        <div class="bg-image"
             style="background-image: url(https://b.zmtcdn.com/data/pictures/7/19488637/d8d726d2a9ec2d3f3ee69a1b3447011c.jpg);">
            <div class="bg-black-50">
                <div class="content content-full text-center">
                    <div class="my-3">
                        <img class="img-avatar img-avatar-thumb"
                             @if(Auth::user()->userDetails->avatar!='')
                             src="{{ asset(Auth::user()->userDetails->avatar ) }}"
                             @else
                             src="{{ asset('assets/backend/assets/media/avatars/avatar13.jpg') }}"
                             @endif
                             alt="{{ Auth::user()->userDetails->name ? Auth::user()->userDetails->name:'Account Photo' }}">
                    </div>
                    <h1 class="h2 text-white mb-0">{{ Auth::user()->userDetails->name }}</h1>
                    <span class="text-white-75">{{ Auth::user()->userDetails->userRole->name }}</span>
                </div>
            </div>
        </div>
        <!-- END Hero -->

        <!-- Stats -->
        <!-- This section is for Chefs Profile only -->
        <div class="bg-white border-bottom">
            <div class="content content-boxed">
                <div class="row items-push text-center">
                    <div class="col-6 col-md-3">
                        <div class="font-size-sm font-w600 text-muted text-uppercase">Sales</div>
                        <a class="link-fx font-size-h3" href="javascript:void(0)">17980</a>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="font-size-sm font-w600 text-muted text-uppercase">Food Items</div>
                        <a class="link-fx font-size-h3" href="javascript:void(0)">27</a>
                    </div>

                    <div class="col-6 col-md-3">
                        <div class="font-size-sm font-w600 text-muted text-uppercase mb-2">739 Ratings</div>
                        <span class="text-warning">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half"></i>
                                </span>
                        <span class="font-size-sm text-muted">(4.9)</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Stats -->

        <!-- Page Content -->
        <div class="content">
            <div class="row">
                <div class="col-xl-12">
                    <div class="block block-rounded">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">My Profile</h3>

                            <div class="block-options">
                                <a href="{{ route('profile.edit') }}" class="btn-block-option">
                                    <i class="fa fa-edit text-success fa-font-icon-size" data-toggle="tooltip"
                                       data-animation="true" data-placement="top"
                                       title="" data-original-title="Edit My Profile"></i>
                                </a>
                            </div>
                        </div>
                        <div class="block-content">
                            <h4 class="font-w300">Name:
                                <small>{{ Auth::user()->userDetails->name  }}</small>
                            </h4>
                            <h4 class="font-w300">Address:
                                <small>{{ Auth::user()->userDetails->address }}</small>
                            </h4>
                            <h4 class="font-w300">Email:
                                <small>{{ Auth::user()->userDetails->email }}</small>
                            </h4>
                            <h4 class="font-w300">Contact:
                                <small>{{ Auth::user()->userDetails->contact }}</small>
                            </h4>
                            <h4 class="font-w300">Role:
                                <small>{{ Auth::user()->userDetails->userRole->name }}</small>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>

            @if(Auth::user()->userDetails->userRole->name==='Chef')
                <div class="row">
                    <div class="col-xl-12">
                        <div class="block block-rounded">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">My Gallery</h3>

                                <div class="block-options">
                                    <button class="btn-block-option" data-toggle="modal"
                                            data-target="#modalGalleryUpload">
                                        <i class="fa fa-cloud-upload-alt text-success fa-font-icon-size"
                                           data-toggle="tooltip"
                                           data-animation="true" data-placement="top"
                                           title="" data-original-title="Upload My Gallery"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="block-content">
                                <div class="row">
                                    @foreach($profileGallery as $valGallery)
                                        <div class="col-md-2 animated fadeIn mb-4 profile-gallery-section">
                                            <div class="options-container">
                                                <img class="img-fluid options-item"
                                                     src="{{ asset($valGallery->image) }}"
                                                     alt="Profile Gallery">
                                                <div class="options-overlay bg-black-75">
                                                    <div class="options-overlay-content">
                                                        <button data-image-id=""
                                                                class="btn btn-sm btn-light btnFoodImageDelete">
                                                            <i class="fa fa-times fa-font-icon-size text-danger"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>


        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->



    <div class="modal" id="modalGalleryUpload" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-rounded block-themed block-transparent mb-0">
                    <form id="frmChefGalleryUpload" action="/dashboard/my-profile/gallery">
                        <div class="block-header bg-primary-dark">
                            {{--<h3 class="block-title">Upload My Gallery</h3>--}}
                            <h3 class="block-title" id="modelTitle" style="color:#fff">Upload My Gallery</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-fw fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content font-size-sm">
                            <div class="form-group">
                                <input type="file" name="images[]" multiple>
                            </div>
                        </div>
                        <div class="block-content block-content-full text-right border-top">
                            <button type="button" class="btn btn-alt-primary mr-1" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" data-form-id="frmChefGalleryUpload"
                                    data-dismiss="modal">Upload
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js_contents')
@endpush
