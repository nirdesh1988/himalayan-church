<div>

    <style>
        #radioBtn .notActive {
            color: #3276b1;
            background-color: #fff;
            border: none;
            box-shadow: none;
        }

        .hidden {
            display: none;
        }

    </style>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="block block-rounded">
                    <div class="block-header">
                        <h3 class="block-title">{{ $page_title }}</h3>
                        <div class="block-options">
                            <div class="block-options-item">
                                <button data-toggle="modal" data-target="#modalAddVerse"
                                        class="btn btn-primary block-title">add
                                </button>
                            </div>
                        </div>
                    </div>

                    @if(session()->has('message'))
                        <div class="block-header">
                            <span class="alert alert-success" id="displayMessage">
                                {{ session()->get('message') }}
                            </span>
                        </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="block-header">
                            <span class="alert alert-danger" id="displayMessage">
                                {{ session()->get('error') }}
                            </span>
                        </div>
                    @endif

                    <div class="block-content">
                        <table class="table table-vcenter">
                            <thead class="thead-light">
                            <tr>
                                <th class="text-center" style="width: 50px;">#</th>
                                <th>Verse</th>
                                <th>Reference</th>
                                <th>Is Display?</th>
                                <th class="text-center" style="width: 100px;">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $valData)
                                <tr>
                                    <th class="text-center" scope="row">{{ ++$key }}</th>
                                    <td class="font-w600 font-size-sm">{{ $valData->verse }}</td>
                                    <td class="d-sm-table-cell">{{ $valData->reference }}</td>
                                    <td>
                                        <div id="radioBtn" class="btn-group">
                                            <a wire:click="checkDisplayStatus('Y', {{ $valData->id }})"
                                               class="btn btn-success btn-sm {{ $valData->display == 'Y'?'active':'notActive' }}"
                                               data-toggle="happy" data-title="yes">YES</a>
                                            <a wire:click="checkDisplayStatus('N', {{ $valData->id }})"
                                               class="btn btn-danger btn-sm {{ $valData->display == 'N'?'active':'notActive' }}"
                                               data-toggle="happy" data-title="no">NO</a>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                        <span data-toggle="tooltip" title="" data-original-title="Edit">
                                            <button wire:click="edit({{ $valData->id }})" data-toggle="modal"
                                                    data-target="#modalUpdateVerse" type="button"
                                                    class="btn btn-sm btn-light js-tooltip-enabled">
                                                <i class="fa fa-edit text-success fa-font-icon-size"></i>
                                            </button>
                                        </span>

                                            <span data-toggle="tooltip" title="" data-original-title="Remove">
                                            <button wire:click="delete({{ $valData->id }})" data-toggle="modal"
                                                    data-target="#modalDeleteVerse" type="button"
                                                    class="btn btn-sm btn-light js-tooltip-enabled">
                                                <i class="fa fa-fw fa-times text-danger fa-font-icon-size"></i>
                                            </button>
                                        </span>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Add Verses-->
    <div class="modal fade" wire:ignore.self id="modalAddVerse" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Add Verse of Day</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="store" id="frmStore">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Verse of the Day</label>
                            <input type="text" wire:model="verse" class="form-control">
                            @error('verse') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Reference</label>
                            <input type="text" wire:model="reference" class="form-control">
                            @error('reference') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary float-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Update Verses-->
    <div class="modal fade" wire:ignore.self id="modalUpdateVerse" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Update Verse of Day</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="update" id="frmUpdate">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Verse of the Day</label>
                            <input type="text" wire:model="verse" class="form-control">
                            @error('verse') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Reference</label>
                            <input type="text" wire:model="reference" class="form-control">
                            @error('reference') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label for="">IsDisplay?</label>--}}
{{--                            <select wire:ignore wire:model="display" class="form-control">--}}
{{--                                <option value="" disabled>Choose option</option>--}}
{{--                                <option wire:key="Y" {{ $display=='Y'?'selected':'' }} value="Y">Yes</option>--}}
{{--                                <option wire:key="N" {{ $display=='N'?'selected':'' }} value="N">No</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary float-right">Update</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <!-- Modal Delete Verses-->
    <div class="modal fade" wire:ignore.self id="modalDeleteVerse" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Delete Verse of Day</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="destroy" id="frmDelete">
                    <div class="modal-body">
                        <p>Are you sure to delete this record?</p>
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-danger float-right">Yes, I confirmed</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>


@push('js_contents')

    <script>
        /* declaring event to close the modal box which need to be emit in livewire controller...*/
        window.livewire.on('verseStore', () => {
            $('#modalAddVerse').modal('hide');
        });
        window.livewire.on('verseUpdate', () => {
            $('#modalUpdateVerse').modal('hide');
        });
        window.livewire.on('verseDelete', () => {
            $('#modalDeleteVerse').modal('hide');
        });

        /* clearing all previous data from form... */
        $('#modalAddVerse').on('hidden.bs.modal', function (e) {
            $('#frmStore')[0].reset();
        })
        $('#modalUpdateVerse').on('hidden.bs.modal', function (e) {
            $('#frmStore')[0].reset();
            $('#frmUpdate')[0].reset();
            $('#frmDelete')[0].reset();
        })
        $('#modalDeleteVerse').on('hidden.bs.modal', function (e) {
            $('#frmStore')[0].reset();
            $('#frmUpdate')[0].reset();
            $('#frmDelete')[0].reset();
        })

        /*
        function to delete data...
        => this function need to be define as listener in livewire controller on which we need to declare with delete function from controller...
        */
        // function deleteReview(id) {
        //     if (confirm("Are you sure to delete this record?"))
        //         window.livewire.emit('deleteReview', id);
        // }

    </script>

@endpush
