<div>
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">{{ $page_title }}</h3>
            <div class="block-options">
                <div class="block-options-item">
                    <button data-toggle="modal" data-target="#modalAddGallery" class="btn btn-primary block-title">add
                    </button>
                </div>
            </div>
        </div>

        <div class="block-content">
            @if(session()->has('message'))
                <div class="alert alert-success" id="displayMessage">
                    {{ session()->get('message') }}
                </div>
            @endif
            <div class="clearfix"></div>
            <table class="table table-vcenter gallery-section">
                <thead class="thead-light">
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Image</th>
                    <th>Tagline</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $key => $valData)
                    <tr>
                        <td class="text-center" scope="row">{{ ++$key }}</td>
                        <td class="font-w600 font-size-sm">
                            <img src="{{ asset('storage/'.$valData->image) }}" alt="{{ $valData->title }}"/>
                        </td>
                        <td class="d-sm-table-cell">{{ $valData->title }}</td>
                        <td class="text-center">
                            <div class="btn-group">
{{--                                <span data-toggle="tooltip" title="" data-original-title="Edit">--}}
{{--                                    <button type="button" class="btn btn-sm btn-light js-tooltip-enabled">--}}
{{--                                        <i class="fa fa-fw fa-pencil-alt text-success fa-font-icon-size"></i>--}}
{{--                                    </button>--}}
{{--                                </span>--}}

                                <span data-toggle="tooltip" title="" data-original-title="Remove">
                                    <button wire:click="delete({{ $valData->id }})"  data-toggle="modal" data-target="#modalDeleteGallery" type="button" class="btn btn-sm btn-light js-tooltip-enabled">
                                        <i class="fa fa-fw fa-times text-danger fa-font-icon-size"></i>
                                    </button>
                                </span>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


    <!-- Modal Add Gallery-->
    <div class="modal fade" wire:ignore.self id="modalAddGallery" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Upload Gallery</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="store" id="frmStore" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Title</label>
                            <input type="text" wire:model="title" class="form-control">
                            @error('title') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Image</label>
                            <input type="file" wire:model="image" class="form-control">
                            @error('image') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary float-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Delete Gallery Modal -->
    <div class="modal fade" wire:ignore.self id="modalDeleteGallery" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Delete Reviews</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="destroy" id="frmDelete">
                    <div class="modal-body">
                        <p>Are you sure to delete this record?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Yes, I confirm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>


@push('js_contents')

    <script>
        /* declaring event to close the modal box which need to be emit in livewire controller...*/
        window.livewire.on('galleryStore', () => {
            $('#modalAddGallery').modal('hide');
        });
        window.livewire.on('galleryDelete', () => {
            $('#modalDeleteGallery').modal('hide');
        });

        /* clearing all previous data from form... */
        $('#modalAddGallery').on('hidden.bs.modal', function (e) {
            $('#frmStore')[0].reset();
        })
        $('#modalDeleteGallery').on('hidden.bs.modal', function (e) {
            $('#frmStore')[0].reset();
            $('#frmDelete')[0].reset();
        })

    </script>

@endpush
