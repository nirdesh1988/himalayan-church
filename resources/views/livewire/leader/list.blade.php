<div>
    <table class="table table-vcenter gallery-section">
        <thead class="thead-light">
        <tr>
            <th class="text-center" style="width: 50px;">#</th>
            <th>Name</th>
            <th>Position</th>
            <th>Information</th>
            <th>Image</th>
            <th class="text-center" style="width: 100px;">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $key => $valData)
            <tr>
                <td class="text-center" scope="row">{{ ++$key }}</td>
                <td class="d-sm-table-cell">{{ $valData->name }}</td>
                <td class="d-sm-table-cell">{{ $valData->position }}</td>
                <td class="d-sm-table-cell">{{ $valData->information }}</td>
                <td class="font-w600 font-size-sm">
                    @if(file_exists('storage/'.$valData->image))
                        <img src="{{ asset('storage/'.$valData->image) }}" alt="{{ $valData->title }}"/>
                    @endif
                </td>
                <td class="text-center">
                    <div class="btn-group">
                        <span data-toggle="tooltip" title="" data-original-title="Edit">
                            <button wire:click="edit({{ $valData->id }})" type="button"
                                    class="btn btn-sm btn-light js-tooltip-enabled">
                                <i class="fa fa-edit text-success fa-font-icon-size"></i>
                            </button>
                        </span>

                        <span data-toggle="tooltip" title="" data-original-title="Remove">
                            <button wire:click="delete({{ $valData->id }})" data-toggle="modal"
                                    data-target="#modalDeleteLeader" type="button"
                                    class="btn btn-sm btn-light js-tooltip-enabled">
                                <i class="fa fa-fw fa-times text-danger fa-font-icon-size"></i>
                            </button>
                        </span>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
