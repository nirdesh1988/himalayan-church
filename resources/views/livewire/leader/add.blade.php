<div>
    <hr>
    <form wire:submit.prevent="store" id="frmStore" enctype="multipart/form-data">
        <div class="modal-body">
            <div class="form-group">
                <label for="">Name</label>
                <input type="text" wire:model="name" class="form-control">
                @error('name') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="">Position</label>
                <input type="text" wire:model="position" class="form-control">
                @error('position') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="">Information</label>
                <input type="text" wire:model="information" class="form-control">
                @error('information') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="">Image</label>
                <input type="file" wire:model="image" class="form-control">
                @error('image') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <button wire:click="closeForm" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary float-right">Save</button>
            </div>
        </div>
    </form>
</div>
