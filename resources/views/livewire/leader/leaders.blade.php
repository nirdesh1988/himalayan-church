<div>
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">{{ $page_title }}</h3>
            <div class="block-options">
                <div class="block-options-item">
                    <button wire:click="create" class="btn btn-primary block-title">add
                    </button>
                </div>
            </div>
        </div>

        <div class="block-content">
            @if(session()->has('message'))
                <div class="alert alert-success" id="displayMessage">
                    {{ session()->get('message') }}
                </div>
            @endif
            <div class="clearfix"></div>

            @if($is_leader_show=='create')
                @include('livewire.leader.add')
            @elseif($is_leader_show=='edit')
                @include('livewire.leader.edit')
            @else
                @include('livewire.leader.list')
            @endif
        </div>
    </div>

    <!-- Delete Gallery Modal -->
    <div class="modal fade" wire:ignore.self id="modalDeleteLeader" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Delete Reviews</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="destroy" id="frmDelete">
                    <div class="modal-body">
                        <p>Are you sure to delete this record?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Yes, I confirm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>


@push('js_contents')

    <script>
        /* declaring event to close the modal box which need to be emit in livewire controller...*/
        window.livewire.on('leaderDelete', () => {
            $('#modalDeleteLeader').modal('hide');
        });

        $('#modalDeleteLeader').on('hidden.bs.modal', function (e) {
            $('#frmStore')[0].reset();
            $('#frmDelete')[0].reset();
        })

    </script>

@endpush
