<div>
    <hr>
    <form wire:submit.prevent="update" id="frmUpdate" enctype="multipart/form-data">
        <div class="modal-body">
            <div class="form-group">
                <label for="">Name</label>
                <input type="text" wire:model="name" class="form-control">
                @error('title') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="">Position</label>
                <input type="text" wire:model="position" class="form-control">
                @error('title') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="">Information</label>
                <input type="text" wire:model="information" class="form-control">
                @error('title') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="">Image</label>
                <input type="file" wire:model="image" class="form-control">
                @if($leaderImage!='')
                    <br>
                    <img src="{{ asset('storage/'.$leaderImage) }}" alt="{{ $name }}" width="120px">
                @endif

                @error('image') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <button wire:click="closeForm" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel
                </button>
                <button type="submit" class="btn btn-primary float-right">Update</button>
            </div>
        </div>
    </form>
</div>
