<div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="block block-rounded">
                    <div class="block-header">
                        <h3 class="block-title">{{ $page_title }}</h3>
                    </div>
                    <div class="block-content">
                        @if(session()->has('message'))
                            <div class="alert alert-success" id="displayMessage">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" id="displayMessage">
                                {{ session()->get('error') }}
                            </div>
                        @endif

                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                                   role="tab" aria-controls="nav-home" aria-selected="true">Primary Configuration</a>
                                <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                                   role="tab" aria-controls="nav-profile" aria-selected="false">Social Connection</a>
                                <a class="nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact"
                                   role="tab" aria-controls="nav-contact" aria-selected="false">SEO </a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                 aria-labelledby="nav-home-tab">
                                <div class="block-options-item mt-3 mb-3">
                                    <button data-toggle="modal" data-target="#modalAddPrimaryData"
                                            class="btn btn-primary block-title">add primary configuration
                                    </button>
                                </div>
                                <table class="table table-vcenter">
                                    <thead class="thead-light">
                                    <tr>
                                        <th width="250">Primary Config Name</th>
                                        <th>Data</th>
                                        <th width="120">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $key => $valData)
                                        @if($valData->description=='primary')
                                            <x-config-row-data :valData="$valData"/>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                 aria-labelledby="nav-profile-tab">
                                <div class="block-options-item mt-3 mb-3">
                                    <button data-toggle="modal" data-target="#modalAddSocial"
                                            class="btn btn-primary block-title">add social conection
                                    </button>
                                </div>
                                <table class="table table-vcenter">
                                    <thead class="thead-light">
                                    <tr>
                                        <th width="250">Social Connection</th>
                                        <th>Data</th>
                                        <th width="120">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $key => $valData)
                                        @if($valData->description=='social')
                                            <x-config-row-data :valData="$valData"/>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                 aria-labelledby="nav-contact-tab">
                                <div class="block-options-item mt-3 mb-3">
                                    <button data-toggle="modal" data-target="#modalAddSeoData"
                                            class="btn btn-primary block-title">add seo data
                                    </button>
                                </div>
                                <table class="table table-vcenter">
                                    <thead class="thead-light">
                                    <tr>
                                        <th width="250">Meta Tag</th>
                                        <th>Data</th>
                                        <th width="120">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $key => $valData)
                                        @if($valData->description=='seo')
                                            <x-config-row-data :valData="$valData"/>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Add Default Config-->
    <div class="modal fade" wire:ignore.self id="modalAddPrimaryData" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Add Primary Configuration</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="storePrimary" id="frmStorePrimary">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Primary Config</label>
                            <input type="text" wire:model="name" class="form-control">
                            @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Value</label>
                            <input type="text" wire:model="value" class="form-control">
                            @error('value') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <button wire:click="closeForm" type="button" class="btn btn-secondary" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary float-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Add Social Connection-->
    <div class="modal fade" wire:ignore.self id="modalAddSocial" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Add Social Connection</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="storeSocial" id="frmStoreSocial">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Social Connection</label>
                            <select wire:model="name" class="form-control">
                                <option value="">Choose Social</option>
                                <option value="facebook">Facebook</option>
                                <option value="google">Google</option>
                                <option value="linkedin">Linkedin</option>
                                <option value="twitter">Twitter</option>
                                <option value="youtube">Youtube</option>
                                <option value="pinterest">Pinterest</option>
                                <option value="instagram">Instagram</option>
                            </select>
                            @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Value</label>
                            <input type="text" wire:model="value" class="form-control">
                            @error('value') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <button wire:click="closeForm" type="button" class="btn btn-secondary" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary float-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Add SEO Meta Data-->
    <div class="modal fade" wire:ignore.self id="modalAddSeoData" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Add SEO Meta Data</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="storeSeo" id="frmStoreSeo">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Meta Tag</label>
                            <select wire:model="name" class="form-control">
                                <option value="">Choose Tag</option>
                                <option value="title">Title</option>
                                <option value="keyword">Keywords</option>
                                <option value="description">Description</option>
                            </select>
                            @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Value</label>
                            <input type="text" wire:model="value" class="form-control">
                            @error('value') <span class="text-danger">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <button wire:click="closeForm" type="button" class="btn btn-secondary" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary float-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>



@push('js_contents')

    <script>
        window.livewire.on('primaryStore', () => {
            $('#modalAddPrimaryData').modal('hide');
        });
        $('#modalAddPrimaryData').on('hidden.bs.modal', function (e) {
            $('#frmStorePrimary')[0].reset();
        })
        window.livewire.on('socialStore', () => {
            $('#modalAddSocial').modal('hide');
        });
        $('#modalAddSocial').on('hidden.bs.modal', function (e) {
            $('#frmStoreSocial')[0].reset();
        })
        window.livewire.on('seoStore', () => {
            $('#modalAddSeoData').modal('hide');
        });
        $('#modalAddSeoData').on('hidden.bs.modal', function (e) {
            $('#frmStoreSeo')[0].reset();
        })

    </script>

@endpush
