<div>
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">{{ $page_title }}</h3>
            <div class="block-options">
                <div class="block-options-item">
                    <a href="{{ route("news.events.add") }}" class="btn btn-primary block-title">add</a>
                </div>
            </div>
        </div>
        <div class="block-content">
            @if(session()->has('success'))
                <div class="alert alert-success" id="displayMessage">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger" id="displayMessage">
                    {{ session()->get('error') }}
                </div>
            @endif
            <div class="clearfix"></div>
            <table class="table table-vcenter">
                <thead class="thead-light">
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Title</th>
                    <th>Short Info</th>
                    <th>Description</th>
                    <th width="130">Image</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $key=> $valData)
                    <tr>
                        <th class="text-center" scope="row">{{ ++$key }}</th>
                        <td class="font-w600 font-size-sm">{{ $valData->title }}</td>
                        <td class="font-w600 font-size-sm">{{ substr($valData->short_info,0,100) }}</td>
                        <td class="font-w600 font-size-sm">{!! html_entity_decode(substr($valData->description,0,150)) !!}</td>
                        <td class="font-w600 font-size-sm">
                            @if(file_exists($valData->image))
                                <img src="{{ asset($valData->image) }}" alt="{{ $valData->title }}" width="120">
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <span data-toggle="tooltip" title="" data-original-title="Edit">
                                    <a href="{{ route("news.events.edit",['id'=>$valData->id]) }}"
                                       class="btn btn-sm btn-light js-tooltip-enabled">
                                        <i class="fa fa-edit text-success fa-font-icon-size"></i>
                                    </a>
                                </span>
                                <span data-toggle="tooltip" title="" data-original-title="Remove">
                                    <button wire:click="delete({{ $valData->id }})" data-toggle="modal"
                                            data-target="#modalDeleteNews" type="button"
                                            class="btn btn-sm btn-light js-tooltip-enabled">
                                        <i class="fa fa-fw fa-times text-danger fa-font-icon-size"></i>
                                    </button>
                                </span>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Delete Gallery Modal -->
    <div class="modal fade" wire:ignore.self id="modalDeleteNews" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Delete News</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="destroy" id="frmDelete">
                    <div class="modal-body">
                        <p>Are you sure to delete this record?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Yes, I confirm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@push('js_contents')

    <script>
        /* declaring event to close the modal box which need to be emit in livewire controller...*/
        window.livewire.on('newsDelete', () => {
            $('#modalDeleteNews').modal('hide');
        });
    </script>

@endpush
