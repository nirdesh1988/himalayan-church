<div>

    <div class="block block-rounded">
        {{--{{ $isUpdateMode }}--}}
        @if($isUpdateMode=='create')
            @include('livewire.testimonials.create')
        @elseif($isUpdateMode=='update')
            @include('livewire.testimonials.update')
        @else

            <div class="block-header">
                <h3 class="block-title">Testimonial</h3>
                <div class="block-options">
                    <div class="block-options-item">
                        <button
                            wire:click="create"
                            {{--data-toggle="modal" data-target="#modalAddTestimonial"--}}
                            class="btn btn-primary block-title">add
                        </button>
                    </div>
                </div>
            </div>

            @if(session()->has('message'))
                <div class="block-header">
                <span class="alert alert-success" id="displayMessage">
                    {{ session()->get('message') }}
                </span>
                </div>
            @endif

            <div class="block-content">
                <table class="table table-vcenter">
                    <thead class="thead-light">
                    <tr>
                        <th class="text-center" style="width: 50px;">#</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Reviews</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key => $valData)
                        <tr>
                            <th class="text-center" scope="row">{{ ++$key }}</th>
                            <td class="font-w600 font-size-sm text-capitalize">{{ $valData->name }}</td>
                            <td class="d-sm-table-cell">{{ $valData->position }}</td>
                            <td class="d-sm-table-cell">{{ $valData->review }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                <span data-toggle="tooltip" data-original-title="Edit">
                                    <button wire:click="edit({{ $valData->id }})" type="button"
                                            class="btn btn-sm btn-light js-tooltip-enabled">
                                                <i class="fa fa-edit text-success fa-font-icon-size"></i>
                                    </button>
                                </span>

                                    <span data-toggle="tooltip" data-original-title="Remove">
                                    <button wire:click="delete({{ $valData->id }})" type="button"
                                            class="btn btn-sm btn-light js-tooltip-enabled">
                                        <i class="fa fa-fw fa-times text-danger fa-font-icon-size"></i>
                                    </button>
                                </span>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>

<!-- Modal -->
<div class="modal fade" wire:ignore.self id="modalAddTestimonial" tabindex="-1" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Testimonial</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form wire:submit.prevent="store">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" wire:model="name" class="form-control">
                        @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Position</label>
                        <input type="text" wire:model="position" class="form-control">
                        @error('position') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Review</label>
                        <textarea wire:model="review" class="form-control" rows="3"></textarea>
                        @error('review') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>


