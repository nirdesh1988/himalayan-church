<div>
    <div class="block-header">
        <h3 class="block-title">Add Testimonial</h3>
    </div>
    <div class="block-content">
        <form wire:submit.prevent="store">
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" wire:model="name" class="form-control">
                    @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="">Position</label>
                    <input type="text" wire:model="position" class="form-control">
                    @error('position') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="">Review</label>
                    <textarea wire:model="review" class="form-control" rows="3" style="resize: none"></textarea>
                    @error('review') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <button wire:click="closeForm" type="button" class="btn btn-secondary">Back</button>
                    <button type="submit" class="btn btn-primary float-right">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
