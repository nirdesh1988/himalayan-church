jQuery(function () {
    var APP_URL = window.location.origin;

    var table = $("#dt_category_details").DataTable({
        paging: false,
        ordering: false,
        pageLength: 20,
        lengthMenu: [
            [20, 50, 100],
            [20, 50, 100]
        ],
        ajax: window.datatables_ajax_url,
        columnDefs: [{className: "text-center", targets: [4]}],
        columns: [
            {data: "id"},
            {data: "name"},
            // {
            //     data: "image",
            //     render: function (data, type, row) {
            //         if (
            //             row.image == "" ||
            //             row.image == null ||
            //             row.image == "null"
            //         ) {
            //             var feat_image =
            //                 APP_URL + "/images/food-placeholder.png";
            //         } else {
            //             var http = new XMLHttpRequest();
            //             var imageUrl = APP_URL + "/" + row.image;
            //             if (imageUrl.length > 0) {
            //                 http.open("HEAD", imageUrl, false);
            //                 http.send();
            //                 if (http.status === 200) {
            //                     feat_image = imageUrl;
            //                 } else {
            //                     feat_image =
            //                         APP_URL + "/images/food-placeholder.png";
            //                 }
            //             }
            //         }
            //         return (
            //             '<img src="' + feat_image + '" alt="' + row.namee + '" width="120">'
            //         );
            //     }
            // },
            {
                data: "status",
                render: function (data, type, row) {
                    var statusData = {
                        1: {
                            title: "Active",
                            class: "font-size-sm font-w600 px-2 py-1 rounded  bg-success-light text-success"
                        },
                        0: {
                            title: "Disabled",
                            class: "font-size-sm font-w600 px-2 py-1 rounded  bg-warning-light text-warning"
                        }
                    };
                    return (
                        '<span class="' + statusData[row.status].class + '">' + statusData[row.status].title + '</span>'
                    );
                }
            },
            {
                data: "id",
                render: function (data, type, row) {
                    return `<div class="btn-group">
                            <button onclick='window.editCategoryForm(${JSON.stringify(row)})' class="btn btn-sm btn-alt-primary">
                                <i data-toggle="tooltip" data-animation="true" data-placement="top" data-original-title="Edit" class="fa fa-fw fa-edit fa-font-icon-size text-success"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-alt-primary" data-toggle="modal" data-id="${row.id}" data-target="#modal_delete_data">
                                <i class="fa fa-fw fa-times fa-font-icon-size text-danger" data-toggle="tooltip" data-animation="true" data-placement="top" data-original-title="Delete"></i>
                            </button>
                        </div>`;
                }
            }
        ]
    });

    table.ajax.reload(null, false); // user paging is not reset on reload

    $("#modal_delete_data").on("show.bs.modal", function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var id = button.data("id"); // Extract info from data-* attributes
        var modal = $(this);
        modal.find(".modal-body input#id").val(id);
    });
});
