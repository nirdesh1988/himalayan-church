jQuery(document).ready(function () {
    var APP_URL = window.location.origin

    var table = $('#dt_promotion_offer_coupon').DataTable({
        "paging": false,
        "ordering": false,
        pageLength: 20,
        lengthMenu: [[20, 50, 100], [20, 50, 100]],
        "ajax": window.datatables_ajax_url,
        columnDefs: [
            {className: "text-center", "targets": [4]}
        ],
        columns: [
            {data: 'id'},
            {data: 'coupon'},
            {data: 'discount_percent'},
            {data: 'expiry_date'},
            {
                data: 'id', render: function (data, type, row) {
                return (
                    '<button type="button" class="btn btn-sm btn-alt-primary" data-toggle="modal" data-id="' + row.id + '" data-target="#modal_delete_data">' +
                    '<i class="fa fa-fw fa-times fa-font-icon-size text-danger" data-toggle="tooltip" data-animation="true" data-placement="top" data-original-title="Delete"></i>' +
                    '</button>'
                );
            }
            }
        ]
    });

    table.ajax.reload(null, false); // user paging is not reset on reload

    $('#modal_delete_data').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        var modal = $(this)
        modal.find('.modal-body input#id').val(id)
    })

});