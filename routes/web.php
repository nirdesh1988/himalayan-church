<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Category\MenuCategory;
use App\Http\Controllers\Auth\AdminLoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/all-clear', function () {
    Artisan::call('config:clear');
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('optimize:clear');
    Artisan::call('storage:link');
    return 'CACHE CONFIG CLEARED';
});

Route::namespace('Frontend')->group(function () {
    Route::get('/', 'MainController@index')->name('front.home');
//    Route::get('/about', 'MainController@about')->name('front.about');
    Route::get('/gallery', 'MainController@gallery')->name('front.gallery');
    Route::get('/newsletter', 'MainController@newsletter')->name('front.newsletter');
    Route::get('/contact', 'MainController@contact')->name('front.contact');
    Route::post('/contact/enquiry', 'MainController@enquiryMail')->name('front.contact.enquiry');

    Route::get('/{slug}', 'MainController@categoryPages')->name('front.page.content');
    //resources section
    Route::get('/resources/{slug}', 'MainController@resourcesBranch')->name('front.resources.branch');

});


//Admin Profile Section
Route::group(['prefix' => 'backend/system', 'as' => 'admin.'], function () {
    Route::get('/login', [AdminLoginController::class, 'showLoginForm'])->name('login');
    Route::post('/login/validate', [AdminLoginController::class, 'login'])->name('login.submit');
    Route::get('/forget-password', [AdminLoginController::class, 'showForgetPasswordForm'])->name('forget.password');
    Route::get('/logout', [AdminLoginController::class, 'logout'])->name('logout');
});

Route::group(['middleware' => ['auth']], function () {
    Route::namespace('Backend')->group(function () {
        Route::group(['prefix' => 'backend/system'], function () {

            Route::group(['prefix' => 'dashboard'], function () {
                Route::get('/', 'MainController@index')->name('dashboard');
            });

            Route::group(['prefix' => 'category'], function () {
                Route::get('/{menu}', 'MenuCategoryController@index')->name('menu.category');
                Route::get('/{menu}/edit/{id}', 'MenuCategoryController@index')->name('menu.category.edit');
                Route::get('/{menu}/{slug}/{id}', 'MenuCategoryController@show')->name('menu.category.show');

                Route::post('/delete', 'MenuCategoryController@destroy')->name('menu.category.delete');
                Route::post('/update/{id}', 'MenuCategoryController@update')->name('menu.category.update');
            });

            Route::group(['prefix' => 'about'], function () {
                Route::get('/', 'AboutController@index')->name('about');
            });

            Route::group(['prefix' => 'church-ministries'], function () {
                Route::get('/', 'ChurchMinistryController@index')->name('ministries');
            });

            Route::group(['prefix' => 'meeting-time'], function () {
                Route::get('/', 'MeetingController@index')->name('meeting');
            });

            Route::group(['prefix' => 'hgst'], function () {
                Route::get('/', 'HgstController@index')->name('hgst');
            });

            Route::group(['prefix' => 'resources'], function () {
                Route::get('/', 'ResourceController@index')->name('resources');

                //resources category
                Route::get('/{slug}/{id}', 'ResourceController@details')->name('resources.category.details');
                Route::get('/{slug}/{id}/add', 'ResourceController@addResources')->name('resources.category.resources.add');
                Route::post('/{slug}/{id}/store', 'ResourceController@storeBranches')->name('resources.category.resources.store');
                Route::get('/{category}/{slug}/{id}/edit', 'ResourceController@editResources')->name('resources.category.resources.edit');
                Route::post('/{branch_id}/update', 'ResourceController@updateBranches')->name('resources.category.resources.update');

                //name-of-resource-slug/unique-id'
                Route::get('/name-of-resource-slug/25145', 'ResourceController@show')->name('resources.show');
            });

            Route::group(['prefix' => 'gallery'], function () {
                Route::get('/', 'GalleryController@index')->name('gallery');
            });

            Route::group(['prefix' => 'verse'], function () {
                Route::get('/', 'VerseController@index')->name('verse');
            });

            Route::group(['prefix' => 'leader'], function () {
                Route::get('/', 'LeaderController@index')->name('leader');
            });

            Route::group(['prefix' => 'news'], function () {
                Route::get('/', 'NewsController@index')->name('news');
                Route::get('/add', 'NewsController@create')->name('news.events.add');
                Route::post('/store', 'NewsController@store')->name('news.events.store');
                Route::get('/edit/{id}', 'NewsController@edit')->name('news.events.edit');
                Route::post('/update/{id}', 'NewsController@update')->name('news.events.update');
            });

            Route::group(['prefix' => 'testimonials'], function () {
                Route::get('/', 'TestimonialController@index')->name('testimonials');
            });

            Route::group(['prefix' => 'system-config'], function () {
                Route::get('/', 'SystemConfigurationController@index')->name('system.config');
            });

            // for sitemap auto generation from admin section only.
            Route::get('/generate-sitemap', function () {
                Artisan::call('sitemap:generate');
                return 'Sitemap Created Successfully'; //Return anything
            })->name('sitemap.generate');

            Route::group(['prefix' => 'db-backup'], function () {
                Route::get('/', 'MainController@dispayData')->name('db.backup');
                Route::get('/generate', 'MainController@exportBackup')->name('db.backup.generate');
                Route::get('download/{filename}', 'MainController@downloadFile')->name('db.backup.dowload');
            });

            //for db backup generation.
            Route::get('/backup', function () {
                Artisan::call('db:backup');
                return 'DATABASE BACKUP CREATED';
            })->name('backup.generate');

        });
    });
    Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
